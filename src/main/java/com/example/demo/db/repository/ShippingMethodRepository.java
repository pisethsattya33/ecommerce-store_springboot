package com.example.demo.db.repository;

import com.example.demo.db.entity.ShippingMethodEntity;
import com.example.demo.model.projection.ShippingMethod.ShippingMethodEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShippingMethodRepository extends JpaRepository<ShippingMethodEntity, Long> {
    @Query("select (count(s) > 0) from ShippingMethodEntity s where s.name = :name and s.deletedAt is null")
    boolean existsByName(@Param("name") String name);

    @Query("select (count(s) > 0) from ShippingMethodEntity s where s.id = ?1 and s.deletedAt is not null")
    boolean existsByIdAndDeleted(Long id);

    @Query("select s from ShippingMethodEntity s where(:query='all'or s.name = :query)and s.deletedAt is null order by s.id desc ")
    Page<ShippingMethodEntityInfo> findByName(@Param("query") String query, Pageable pageable);


}