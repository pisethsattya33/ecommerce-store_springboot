package com.example.demo.db.repository;


import com.example.demo.db.entity.UserPaymentMethodEntity;
import com.example.demo.db.status.Provider;
import com.example.demo.model.projection.UserPayment.UserPaymentMethodInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface UserPaymentMethodRepository extends JpaRepository<UserPaymentMethodEntity, Long> {

    @Query("select (count(u) > 0) from UserPaymentMethodEntity u where upper(u.accountNumber) = upper(?1)")
    boolean existAccountNumber(String accountNumber);

    @Query("""
     select u from UserPaymentMethodEntity u left join u.user as  users left join u.paymentType as payment_id 
   where (:provider = 'ALL' or u.provider = :provider and u.accountNumber is not null)And u.deletedAt is null order by u.isDefault DESC """)
   Page<UserPaymentMethodInfo> findUserPayment( Provider provider, Pageable pageable);

    @Query("select u from UserPaymentMethodEntity u where u.user.id = :id and u.deletedAt is null order by u.isDefault DESC ")
    List<UserPaymentMethodInfo> findByUser_IdAndIsDefaultNull(@Param("id") Long id);

    @Query("select (count(u) > 0) from UserPaymentMethodEntity u where u.id = ?1 and u.deletedAt is not null")
    boolean alreadyDelete(Long id);

//    @Query("select u from UserPaymentMethodEntity u where u.user.id And u.isDefault = true")
//    UserPaymentMethodEntity findByProviderAndIsDefaultTrue(Long id);

    @Query("select u from UserPaymentMethodEntity u where u.user.id = :id and u.isDefault = true")
    UserPaymentMethodEntity findByProviderAndIsDefaultTrue(@Param("id") Long id);


    @Query("select u from UserPaymentMethodEntity u where u.id = :id and u.isDefault = true and u.deletedAt is null")
    UserPaymentMethodEntity findByIdAndIsDefaultTrueAndDeletedAtNull(@Param("id") Long id);

    @Query("select u from UserPaymentMethodEntity u where u.user.id = :id and u.isDefault = true")
    UserPaymentMethodEntity findByUser_IdAndIsDefaultTrue(@Param("id") Long id);

    UserPaymentMethodEntity findByUser_IdAndIsDefaultFalse(Long id);

    List<UserPaymentMethodEntity> findAllByUserId(Long id);

    @Query("select u from UserPaymentMethodEntity u where (u.id = :id and u.isDefault = true) and u.deletedAt is NULL ")
    UserPaymentMethodEntity findByIdAndIsDefaultTrue(@Param("id") Long id);


}
