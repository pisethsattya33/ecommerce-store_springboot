package com.example.demo.db.repository;

import com.example.demo.db.entity.ShoppingCartEntity;
import com.example.demo.db.entity.UserEntity;
import com.example.demo.model.projection.ShoppingCart.ShoppingCartEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCartEntity, Long> {
    @Query("select s from ShoppingCartEntity s left join fetch s.shoppingCartItems left join fetch s.user where s.user.id = :userid and s.deletedAt is null")
    Page<ShoppingCartEntityInfo> findByUser_IdAndDeletedAtNull(@Param("userid") Long userid, Pageable pageable);

    @Query("SELECT s FROM ShoppingCartEntity s WHERE s.user = :user and s.deletedAt is null")
    ShoppingCartEntity findByUser(@Param("user") UserEntity user);

    @Query("SELECT s FROM ShoppingCartEntity s LEFT JOIN FETCH s.shoppingCartItems WHERE s.user.id = :id")
    ShoppingCartEntity findByUser(@Param("id") Long id);

    @Query("select s from ShoppingCartEntity s where s.user.id = :id and s.deletedAt is null ")
    ShoppingCartEntity findByUser_Id(@Param("id") Long id);


}