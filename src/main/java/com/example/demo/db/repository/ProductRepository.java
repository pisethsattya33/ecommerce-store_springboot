package com.example.demo.db.repository;

import com.example.demo.db.entity.ProductEntity;
import com.example.demo.model.projection.product.ProductEntityInfo;
import com.example.demo.model.projection.product.ProductUserReviewEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author Sattya
 * create at 1/27/2024 3:52 PM
 */
public interface ProductRepository extends JpaRepository<ProductEntity,Long> {
//    @Query("""
//            select p from ProductEntity p left join fetch p.productItems
//            where (:query = 'ALL' or lower(p.uuid) like concat('%',lower(:query),'%') or lower(p.name) like concat('%',lower(:query),'%') or lower(p.category.name) like concat('%',lower(:query),'%')) and p.deletedAt is null""")
//    Page<ProductEntityInfo> findByQuery(@Param("query") String query, Pageable pageable);
    @Query("""
        select p from ProductEntity p left join fetch p.productItems
        where (:query = 'all' or p.name = :query or p.category.name = :query) and p.deletedAt is null
        """)
    Page<ProductEntityInfo> findByQuery(@Param("query") String query, Pageable pageable);


    @Query("select (count(p) > 0) from ProductEntity p where p.uuid = :uuid and p.deletedAt is null")
    boolean existsByUuidAndDeletedAtIsNull(@Param("uuid") String uuid);

    @Query("select p from ProductEntity p where p.uuid = :uuid")
    ProductEntity findByUuid(@Param("uuid") String uuid);

    @Query("select (count(p) > 0) from ProductEntity p where p.name = :name and p.deletedAt is null")
    boolean existsByName(@Param("name") String name);

    @Query("select (count(p) > 0) from ProductEntity p where p.uuid = :uuid")
    boolean existsByUuid(@Param("uuid") String uuid);

    @Query("select p from ProductEntity p left join fetch  p.userReviews as ur where p.uuid = :uuid and p.deletedAt is null and ur.deletedAt is null")
    Page<ProductUserReviewEntityInfo> findByUuidAndDeletedAtNull(@Param("uuid") String uuid, Pageable pageable);


}
