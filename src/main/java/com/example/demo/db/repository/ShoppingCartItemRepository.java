package com.example.demo.db.repository;

import com.example.demo.db.entity.ProductItemEntity;
import com.example.demo.db.entity.ShoppingCartEntity;
import com.example.demo.db.entity.ShoppingCartItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ShoppingCartItemRepository extends JpaRepository<ShoppingCartItemEntity, Long> {

@Query("select s from ShoppingCartItemEntity s where (s.shoppingCart = :shoppingCart and s.productItem = :productItem) and s.deletedAt is null ")
ShoppingCartItemEntity findByShoppingCartAndProductItem(@Param("shoppingCart") ShoppingCartEntity shoppingCart, @Param("productItem") ProductItemEntity productItem);
@Query("SELECT s FROM ShoppingCartItemEntity s WHERE (s.shoppingCart.id = :cartId) and s.deletedAt is null")
List<ShoppingCartItemEntity> findByShoppingCartId(@Param("cartId") Long cartId);


}