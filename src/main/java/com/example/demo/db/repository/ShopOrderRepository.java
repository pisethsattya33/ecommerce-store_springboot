package com.example.demo.db.repository;

import com.example.demo.db.entity.OrderLineEntity;
import com.example.demo.db.entity.ShopOrderEntity;
import com.example.demo.model.projection.ShopOrder.ShopOrderEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShopOrderRepository extends JpaRepository<ShopOrderEntity, Long> {
    @Query("select s from ShopOrderEntity s where s.id = :id and s.deletedAt is null")
    ShopOrderEntity findByIdAndDeletedAtNull(@Param("id") Long id);

    @Query("""
            select s from ShopOrderEntity s left join fetch s.orderLines orderLines inner join fetch s.orderStatus orderstatus
            where (:query='all'or orderLines.id = :id or orderstatus.status=:query) and s.deletedAt is null order by s.orderStatus.id asc """)
    Page<ShopOrderEntityInfo> findByOrderLines_Id(@Param("id") Long id, @Param("query") String query, Pageable pageable);

    boolean existsByIdAndDeletedAtNull(Long id);

    ShopOrderEntity findByOrderLines(OrderLineEntity orderLines);

    @Query("""
             select s from ShopOrderEntity s  left join fetch s.orderLines orderLines inner join fetch s.orderStatus orderstatus
             where s.user.id = :id and s.deletedAt is null order by s.orderStatus.id asc """)
    Page<ShopOrderEntityInfo> findByUser_IdAndDeletedAtNull(@Param("id") Long id, Pageable pageable);


}