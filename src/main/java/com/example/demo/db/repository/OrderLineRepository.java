package com.example.demo.db.repository;

import com.example.demo.db.entity.OrderLineEntity;
import com.example.demo.model.projection.Orderline.OrderLineEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

public interface OrderLineRepository extends JpaRepository<OrderLineEntity, Long> {
    @Query("select o from OrderLineEntity o where (:query='all'or o.id = :order_id ) and o.deletedAt is null")
    Page<OrderLineEntityInfo> findByIdAndDeletedAtNull(@Param("order_id") Long id,String query ,Pageable pageable);

    @Query("select (count(o) > 0) from OrderLineEntity o where o.id = :id and o.deletedAt is not null")
    boolean existsByDeleted(@Param("id") Long id);

    @Query("select o from OrderLineEntity o where o.shopOrder.id in :ids")
    List<OrderLineEntity> findByShopOrder_Id(@Param("ids") Set<Long> ids);


}