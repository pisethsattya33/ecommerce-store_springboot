package com.example.demo.db.repository;

import com.example.demo.db.entity.UserReviewEntity;
import com.example.demo.model.projection.UserReview.UserReviewEntityInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
public interface UserReviewRepository extends JpaRepository<UserReviewEntity, Long> {

@Query("SELECT u FROM UserReviewEntity u left join fetch u.products WHERE (:ratingValue=0 or u.ratingValue = :ratingValue) AND u.deletedAt IS NULL")
Page<UserReviewEntityInfo> findByRatingValueAndDeletedAtNull(@Param("ratingValue") Double ratingValue, Pageable pageable);
    boolean existsByIdAndDeletedAtNotNull(Long id);


}