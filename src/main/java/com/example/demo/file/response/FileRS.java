package com.example.demo.file.response;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.Date;

/**
 * @author Sattya
 * create at 2/12/2024 12:30 PM
 */
@Data
@Builder
public class FileRS {
    private String id;
    private String name;
    private String uri;
    private Long size;
    private String downloadUri;
    private String extension;
    private Instant date;
    private String time;
}
