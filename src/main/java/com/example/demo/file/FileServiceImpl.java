package com.example.demo.file;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.config.file.S3Config;
import com.example.demo.config.property.S3Buckets;
import com.example.demo.constant.MessageConstant;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.file.response.FileRS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;


/**
 * @author Sattya
 * create at 2/29/2024 10:21 AM
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class FileServiceImpl extends BaseService implements FileService {

    private final S3Config s3Config;
    private final AmazonS3 amazonS3;
    private final S3Buckets s3Buckets;

    @Override
    public FileRS downloadByName(String name) {
        String s3BucketName =s3Buckets.getName();

        String objectKey=name;

        ObjectMetadata metadata=amazonS3.getObjectMetadata(s3BucketName,objectKey);

        Long size=metadata.getContentLength();

        GeneratePresignedUrlRequest generatePresignedUrlRequest= new GeneratePresignedUrlRequest(s3BucketName,objectKey);
        URL downloaduri=amazonS3.generatePresignedUrl(generatePresignedUrlRequest);

        String fileID= UUID.randomUUID().toString();
        Date uploadDate = metadata.getLastModified();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedTime = timeFormat.format(uploadDate);

        FileRS fileRp=FileRS.builder()
                .id(fileID)
                .name(objectKey)
                .size(size)
                .downloadUri(downloaduri.toString())
                .date(uploadDate.toInstant())
                .time(formattedTime)
                .extension(getExtension(objectKey))
                .build();
        return fileRp;

    }


    @Override
    public StructureRS deleteAll() {
        String bucketName= s3Buckets.getName();
        ObjectListing objectListing = amazonS3.listObjects(bucketName);
        for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
            String key = objectSummary.getKey();
            amazonS3.deleteObject(new DeleteObjectRequest(bucketName, key));
            System.out.println("Deleted object with key: " + key + " from bucket: " + bucketName);
        }
       return response();

    }

    @Override
    public StructureRS deleteByName(String name) {
        String s3BucketName = s3Buckets.getName();
        String objectKey = name;

        // Check if the bucket exists
        boolean bucketExists = amazonS3.doesBucketExistV2(s3BucketName);
        if (!bucketExists) {
            throw new RuntimeException("S3 bucket does not exist");
        }else {
            DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(s3BucketName, objectKey);
            amazonS3.deleteObject(deleteObjectRequest);
            return response();
        }

    }



    @Override
    public StructureRS findAll() {
        List<FileRS> fileList = new ArrayList<>();
//        ListObjectsRequest listObjectsRequest=new ListObjectsRequest()
//                .withBucketName(s3Buckets.getName())
//                .withPrefix(s3Buckets.getName());
//
        ObjectListing objectListing = amazonS3.listObjects(s3Buckets.getName());

        List<S3ObjectSummary> objectSummaries = objectListing.getObjectSummaries();
        Collections.sort(objectSummaries, Comparator.comparing(S3ObjectSummary::getKey).reversed());

        for (S3ObjectSummary objectSummary : objectSummaries) {
            String fileName = objectSummary.getKey();

            String uri = amazonS3.getUrl(s3Buckets.getName(), fileName).toString();
            long fileSize = objectSummary.getSize();
            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(s3Buckets.getName(), fileName);
            generatePresignedUrlRequest.setMethod(HttpMethod.GET);

            URL downloadUrl = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);

            String fileID= UUID.randomUUID().toString();
            Date uploadDate = objectSummary.getLastModified();
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            String formattedTime = timeFormat.format(uploadDate);

            FileRS fileResponse = FileRS.builder()
                    .id(fileID)
                    .name(fileName)
                    .uri(uri)
                    .size(fileSize)
                    .downloadUri(downloadUrl.toString())
                    .extension(getExtension(fileName))
                    .date(uploadDate.toInstant())
                    .time(formattedTime)
                    .build();
            fileList.add(fileResponse);
        }
        return response(fileList);
    }


    @Override
    public FileRS findByName(String name) throws IOException {
        String s3BucketName = s3Buckets.getName();
        String objectKey = name;
        // Retrieve the object metadata
        ObjectMetadata objectMetadata = amazonS3.getObjectMetadata(s3BucketName, objectKey);
        // Get file size
        Long fileSize = objectMetadata.getContentLength();

        // Generate a presigned URL for downloading the specific object
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(s3BucketName, objectKey);
        URL downloadUrl = amazonS3.generatePresignedUrl(generatePresignedUrlRequest);

        // Get content type (file extension) based on the object key
        String contentType = getExtension(objectKey);

        // Construct the URI for the object
        String uri = amazonS3.getUrl(s3BucketName, objectKey).toString();

        // Create a FileRS object using builder pattern
        String fileID= UUID.randomUUID().toString();
        Date uploadDate = objectMetadata.getLastModified();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedTime = timeFormat.format(uploadDate);

        FileRS fileResponse = FileRS.builder()
                .id(fileID)
                .name(objectKey)
                .uri(uri)
                .size(fileSize)
                .downloadUri(downloadUrl.toString())
                .extension(getExtension(contentType))
                .date(uploadDate.toInstant())
                .time(formattedTime)
                .build();
        return fileResponse;
    }


    @Override
    public FileRS uploadSingle(MultipartFile file) throws IOException {
        if(file.isEmpty()){
            throw new RuntimeException(MessageConstant.FILE.FILE_EMPTY);
        }
        return save(file);
    }
    private FileRS save(MultipartFile file)throws IOException{
        File files = s3Config.convertMultiPartToFile(file);
        String fileName = s3Config.generateFileName(file);

        String contentType = getExtension(fileName);

        String uri = amazonS3.getUrl(s3Buckets.getName(), fileName).toString();
        if (file.isEmpty()){
            throw new NotFoundException(MessageConstant.FILE.FILE_EMPTY);
        }
        if (file == null){
            throw new NotFoundException(MessageConstant.FILE.FILE_EMPTY);
        }
       if (!file.getContentType().equals("image/jpeg")&&!file.getContentType().equals("image/jpg")&&!file.getContentType().equals("image/png")&&!file.getContentType().equals("image/webp")){
           throw new BadRequestException(MessageConstant.FILE.FILE_FORMAT_CORRECT);
       }
        if (file.getSize() > 1024 * 1024 * 10) {
            throw new BadRequestException(MessageConstant.FILE.FILE_SIZE_LIMIT);
        }
        amazonS3.putObject(new PutObjectRequest(s3Buckets.getName(), fileName, files)
                .withCannedAcl(CannedAccessControlList.PublicRead));
        files.delete();

        ObjectMetadata metadata = amazonS3.getObjectMetadata(s3Buckets.getName(), fileName);
        Long fileSize = metadata.getContentLength();
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(s3Buckets.getName(), fileName);
        URL downloadurl=amazonS3.generatePresignedUrl(generatePresignedUrlRequest);

        String fileID= UUID.randomUUID().toString();
        Date uploadDate = metadata.getLastModified();
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedTime = timeFormat.format(uploadDate);

        FileRS fileResponse = FileRS.builder()
                .id(fileID)
                .name(fileName)
                .uri(uri)
                .size(fileSize)
                .downloadUri(downloadurl.toString())
                .extension(contentType)
                .date(uploadDate.toInstant())
                .time(formattedTime)
                .build();
        fileResponse.setSize(fileSize);
        return fileResponse;
    }
    @Override
    public List<FileRS> uploadMultiple(List<MultipartFile> files) {
        return files.stream().map(file -> {
            try {
                return save(file);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }).toList();
    }

    private String getExtension(String fileName) {
        // Get file extension
        int lastDotIndex = fileName.lastIndexOf(".");
        return fileName.substring(lastDotIndex + 1);
    }

}
