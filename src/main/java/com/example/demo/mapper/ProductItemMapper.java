package com.example.demo.mapper;

import com.example.demo.db.entity.ProductItemEntity;
import com.example.demo.model.request.product_item.CreateProductItemRQ;
import com.example.demo.model.request.product_item.UpdateProductItemRQ;
import com.example.demo.model.response.productItems.ProductItemEntityDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

/**
 * @author Sattya
 * create at 2/10/2024 8:53 PM
 */
@Mapper(componentModel = "spring")
public interface ProductItemMapper {
    @Mapping(target = "product.id", source = "productId")
    ProductItemEntity fromCreateProductItemRQ(CreateProductItemRQ request);

    ProductItemEntityDto toDto(ProductItemEntity productItemEntity);
    @BeanMapping(nullValuePropertyMappingStrategy = org.mapstruct.NullValuePropertyMappingStrategy.IGNORE)
    void fromUpdateProductItemRQ(@MappingTarget ProductItemEntity productItemEntity, UpdateProductItemRQ request);
}
