package com.example.demo.model.response.product;

import jakarta.validation.constraints.Size;

import java.io.Serializable;

/**
 * DTO for {@link com.example.demo.db.entity.ProductEntity}
 */
public record ProductEntityDto(Long id, String uuid, @Size(max = 150) String name, String description, String image,
                               CategoryEntityDto category) implements Serializable {
    /**
     * DTO for {@link com.example.demo.db.entity.CategoryEntity}
     */
    public record CategoryEntityDto(Long id, String uuid, String name, String description,
                                    CategoryEntityDto1 parent) implements Serializable {
        /**
         * DTO for {@link com.example.demo.db.entity.CategoryEntity}
         */
        public record CategoryEntityDto1(Long id, String uuid, String name, String description) implements Serializable {
        }
    }
}