package com.example.demo.model.request.ShippingMethod;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class UpdateShippingMethodRQ {
    @NotEmpty(message = "Please provide name ")
    @NotNull(message = "Please provide name ")
    private String name;
    private BigDecimal price;
}
