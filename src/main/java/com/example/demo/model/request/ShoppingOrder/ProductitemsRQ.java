package com.example.demo.model.request.ShoppingOrder;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
public class ProductitemsRQ {
    @NotEmpty(message = "Please Provide Product Item Id")
    private Long product_item_id;
    @NotNull(message = "Please Provide Quantity")
    private Integer qty;
}
