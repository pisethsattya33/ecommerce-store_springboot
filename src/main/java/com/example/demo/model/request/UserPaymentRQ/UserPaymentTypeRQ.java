package com.example.demo.model.request.UserPaymentRQ;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;

@Getter
@Setter
public class UserPaymentTypeRQ {
    @NotNull(message = "Value must not empty")
    private String value;
}
