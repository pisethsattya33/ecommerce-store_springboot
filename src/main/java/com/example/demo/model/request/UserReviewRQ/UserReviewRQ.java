package com.example.demo.model.request.UserReviewRQ;

import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Range;


import java.util.Set;

@Getter
@Setter
public class UserReviewRQ {
@NotNull(message = "please make sure order product id not empty")
Set<@Positive Long>product_Id;

@NotNull(message = "need rate")
private double ratingValue;

@NotEmpty(message = "Please Write Some Comment on Product")
private String comment;

private String images;
}
