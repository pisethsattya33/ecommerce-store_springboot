package com.example.demo.model.request.Orderline;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateStatusOrderlineRQ {
    @NotNull(message = "status is required")
    private Boolean status;
}
