package com.example.demo.model.request.ShoppingOrder;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class ShoppingCartOrderRQ {
    @NotNull(message = "Please provide shipping id")
    Long shippingMethod;
    @NotNull(message = "Please provide pay method")
    Long user_payment_method;
    @NotEmpty(message = "Please provide shipping address")
    private String shippingAddress;
    Set<CartItemRQ> cartItems;
}
