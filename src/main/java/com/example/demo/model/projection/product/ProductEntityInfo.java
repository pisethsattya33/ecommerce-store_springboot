package com.example.demo.model.projection.product;

import com.example.demo.model.projection.productItem.ProductItemEntityInfo;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Set;

/**
 * Projection for {@link com.example.demo.db.entity.ProductEntity}
 */
public interface ProductEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    String getUuid();

    String getName();

    String getDescription();

    String getImage();

    CategoryEntityInfo getCategory();

    List<ProductItemEntityInfo> getProductItems();

    /**
     * Projection for {@link com.example.demo.db.entity.CategoryEntity}
     */
    interface CategoryEntityInfo {
        Long getId();

//        String getUuid();

        String getName();

        CategoryEntityInfo1 getParent();

        /**
         * Projection for {@link com.example.demo.db.entity.CategoryEntity}
         */
        interface CategoryEntityInfo1 {
            Long getId();

//            String getUuid();

            String getName();
        }
    }

    /**
     * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
     */
    interface ProductItemEntityInfo {
        Long getId();

        BigDecimal getPrice();
//        Set<VariationOptionEntityInfo> getVariationOptions();
    }
    /**
     * Projection for {@link com.example.demo.db.entity.VariationOptionEntity}
     */
    interface VariationOptionEntityInfo {
        Long getId();

        String getValue();

        com.example.demo.model.projection.productItem.ProductItemEntityInfo.VariationOptionEntityInfo.VariationEntityInfo getVariation();

        /**
         * Projection for {@link com.example.demo.db.entity.VariationEntity}
         */
        interface VariationEntityInfo {
//            Long getId();

            String getName();
        }
    }
}