package com.example.demo.model.projection.ShopOrder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;

/**
 * Projection for {@link com.example.demo.db.entity.ShopOrderEntity}
 */
public interface ShopOrderEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    Date getOrderDate();

    BigDecimal getOrderTotal();

    String getShippingAddress();

    List<OrderLineEntityInfo> getOrderLines();

    ShippingMethodEntityInfo getShippingMethod();

    OrderStatusEntityInfo getOrderStatus();

    /**
     * Projection for {@link com.example.demo.db.entity.OrderLineEntity}
     */
    interface OrderLineEntityInfo {
        Long getId();

        Integer getQuantity();

        BigDecimal getPrice();

        ProductItemEntityInfo getProductItem();

        /**
         * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
         */
        interface ProductItemEntityInfo {
            String getImage();

            ProductEntityInfo getProduct();

            /**
             * Projection for {@link com.example.demo.db.entity.ProductEntity}
             */
            interface ProductEntityInfo {
                String getName();

                CategoryEntityInfo getCategory();

                /**
                 * Projection for {@link com.example.demo.db.entity.CategoryEntity}
                 */
                interface CategoryEntityInfo {
                    String getName();

                    CategoryEntityInfo1 getParent();

                    /**
                     * Projection for {@link com.example.demo.db.entity.CategoryEntity}
                     */
                    interface CategoryEntityInfo1 {
                        String getName();
                    }
                }
            }
        }
    }

    /**
     * Projection for {@link com.example.demo.db.entity.ShippingMethodEntity}
     */
    interface ShippingMethodEntityInfo {
        String getName();

        BigDecimal getPrice();
    }

    /**
     * Projection for {@link com.example.demo.db.entity.OrderStatusEntity}
     */
    interface OrderStatusEntityInfo {
        Long getId();

        String getStatus();
    }
}