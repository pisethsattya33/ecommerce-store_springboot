package com.example.demo.model.projection.UserPayment;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.PaymentTypeEntity}
 */
public interface PaymentTypeEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();
    String getValue();
}