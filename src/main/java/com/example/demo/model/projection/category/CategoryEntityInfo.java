package com.example.demo.model.projection.category;

/**
 * Projection for {@link com.example.demo.db.entity.CategoryEntity}
 */
public interface CategoryEntityInfo {
    Long getId();

    String getUuid();

    String getName();

    String getDescription();

    CategoryEntityInfo1 getParent();

    /**
     * Projection for {@link com.example.demo.db.entity.CategoryEntity}
     */
    interface CategoryEntityInfo1 {
        Long getId();

        String getName();

    }
}