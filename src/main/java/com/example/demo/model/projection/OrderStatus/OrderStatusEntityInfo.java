package com.example.demo.model.projection.OrderStatus;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.OrderStatusEntity}
 */
public interface OrderStatusEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    String getStatus();
}