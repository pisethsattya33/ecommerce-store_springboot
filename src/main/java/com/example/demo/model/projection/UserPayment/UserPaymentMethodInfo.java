package com.example.demo.model.projection.UserPayment;

import com.example.demo.db.status.Provider;

import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.UserPaymentMethodEntity}
 */
public interface UserPaymentMethodInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();
    Provider getProvider();

    String getAccountNumber();

    String getExpiryDate();

    Boolean getIsDefault();

    UserEntityInfo getUser();

    PaymentTypeEntityInfo getPaymentType();

    /**
     * Projection for {@link com.example.demo.db.entity.UserEntity}
     */
    interface UserEntityInfo {
        String getName();
    }

    /**
     * Projection for {@link com.example.demo.db.entity.PaymentTypeEntity}
     */
    interface PaymentTypeEntityInfo  {
        String getValue();
    }
}