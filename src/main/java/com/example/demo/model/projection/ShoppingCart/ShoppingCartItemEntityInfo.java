package com.example.demo.model.projection.ShoppingCart;

import java.math.BigDecimal;

/**
 * Projection for {@link com.example.demo.db.entity.ShoppingCartItemEntity}
 */
public interface ShoppingCartItemEntityInfo {
    Long getId();

    Integer getQuantity();

    ProductItemEntityInfo getProductItem();

    ShoppingCartEntityInfo1 getShoppingCart();

    /**
     * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
     */
    interface ProductItemEntityInfo {
        Long getId();

        String getImage();

        BigDecimal getPrice();
    }

    /**
     * Projection for {@link com.example.demo.db.entity.ShoppingCartEntity}
     */
    interface ShoppingCartEntityInfo1 {
        Long getId();

        UserEntityInfo getUser();

        /**
         * Projection for {@link com.example.demo.db.entity.UserEntity}
         */
        interface UserEntityInfo {
            Long getId();

            String getName();
        }
    }
}