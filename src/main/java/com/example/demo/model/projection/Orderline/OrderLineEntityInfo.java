package com.example.demo.model.projection.Orderline;

import java.math.BigDecimal;
import java.time.Instant;

/**
 * Projection for {@link com.example.demo.db.entity.OrderLineEntity}
 */
public interface OrderLineEntityInfo {
    Instant getCreatedAt();

    Instant getUpdatedAt();

    Long getId();

    Integer getQuantity();

    BigDecimal getPrice();

    ProductItemEntityInfo getProductItem();

    /**
     * Projection for {@link com.example.demo.db.entity.ProductItemEntity}
     */
    interface ProductItemEntityInfo {
        Long getId();

        String getCode();

//        Integer getQuantity();

//        String getImage();

        BigDecimal getPrice();

        ProductEntityInfo getProduct();

        /**
         * Projection for {@link com.example.demo.db.entity.ProductEntity}
         */
        interface ProductEntityInfo {
            Long getId();

            String getName();

            String getImage();
        }
    }
}