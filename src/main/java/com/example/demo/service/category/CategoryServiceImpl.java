package com.example.demo.service.category;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.CategoryEntity;
import com.example.demo.db.repository.CategoryRepository;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.mapper.CategoryEntityMapper;
import com.example.demo.model.projection.category.CategoryEntityInfo;
import com.example.demo.model.request.category.CategoryDto;
import com.example.demo.model.request.category.UpdateCategoryRQ;
import com.example.demo.model.request.category.UpdateStatusCategoryRQ;
import com.example.demo.model.response.category.CategoryEntityDto;
import com.example.demo.model.response.category.CategoryItemRS;
import com.example.demo.model.response.category.CategoryRS;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Sattya
 * create at 2/14/2024 4:23 PM
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class CategoryServiceImpl extends BaseService implements CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryEntityMapper categoryEntityMapper;
    @Transactional
    @Override
    public StructureRS createNew(CategoryDto request) {
        try {
            if (categoryRepository.existsByName(request.name())) {
                throw new InternalServerError(MessageConstant.CATEGORY.CATEGORY_ALREADY_EXISTS);
            }
            CategoryEntity categoryEntity = categoryEntityMapper.toEntity(request);
            categoryEntity.setUuid(UUID.randomUUID().toString());
            if (request.parentId() != null && categoryRepository.existsById(request.parentId())) {
                categoryEntity.setParent(categoryRepository.findById(request.parentId()).orElseThrow(() -> new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND)));
                categoryRepository.save(categoryEntity);
            }
            else if (request.parentId() == null){
                categoryEntity.setId(null);
                categoryRepository.save(categoryEntity);
            }else {
                throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
            }
        } catch (Exception e) {
            // Log the exception or handle it according to your application's requirements
            // For now, let's print the stack trace
            e.printStackTrace();
            // You might also throw a custom exception or rollback the transaction if needed
            throw new InternalServerError(e.getMessage());
        }
        return response(MessageConstant.CATEGORY.CATEGORY_CREATED_SUCCESSFULLY);
    }


    @Override
    public StructureRS findByUuid(String uuid, BaseListingRQ request) {
       if (!categoryRepository.existsByUuidAndDeletedAtIsNull(uuid)){
           throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
       }
    CategoryEntityDto categoryEntityDto = categoryEntityMapper.toDto1(categoryRepository.findByUuid(uuid));
        return response(categoryEntityDto);
    }
    @Transactional
    @Override
    public StructureRS updateStatusCategories(String uuid,UpdateStatusCategoryRQ request) {
        boolean isFound = categoryRepository.existsByUuid(uuid);
        if (!isFound){
            throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
        }
        Instant deletedAt = request.getStatus() ? null : Instant.now();
        categoryRepository.findByUuid(uuid).setDeletedAt(deletedAt);

        String message = request.getStatus() ? MessageConstant.CATEGORY.CATEGORY_RESTORED_SUCCESSFULLY : MessageConstant.CATEGORY.CATEGORY_DELETED_SUCCESSFULLY;
        return response(message);
    }


    @Transactional
    @Override
    public void updateByUuid(String uuid, UpdateCategoryRQ request) {
        CategoryEntity categoryEntity = getCategoryByUuid(uuid);
        checkCategoryExistsByName(request.getName());
        partialUpdateCategory(request, categoryEntity);
        updateParentCategory(request.getParentId(), categoryEntity);
        saveCategory(categoryEntity);
    }

    private CategoryEntity getCategoryByUuid(String uuid) {
        if (!categoryRepository.existsByUuidAndDeletedAtIsNull(uuid)) {
            throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
        }
        return categoryRepository.findByUuid(uuid);
    }

    private void checkCategoryExistsByName(String name) {
        if (categoryRepository.existsByName(name)) {
            throw new InternalServerError(MessageConstant.CATEGORY.CATEGORY_ALREADY_EXISTS);
        }
    }

    private void partialUpdateCategory(UpdateCategoryRQ request, CategoryEntity categoryEntity) {
        categoryEntityMapper.partialUpdate(request, categoryEntity);
    }

    private void updateParentCategory(Long parentId, CategoryEntity categoryEntity) {
        if (parentId != null && categoryRepository.existsById(parentId) ) {
            if (categoryEntity.getId().equals(parentId)){
                throw new InternalServerError(MessageConstant.CATEGORY.CATEGORY_CANNOT_BE_PARENT_OF_ITSELF);
            }
            categoryEntity.setParent(categoryRepository.findById(parentId).orElseThrow(() -> new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND)));
        } else if (parentId == null){
            categoryEntity.setParent(null);
        }
        else {
            throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
        }
    }

    private void saveCategory(CategoryEntity categoryEntity) {
        categoryRepository.save(categoryEntity);
    }

    @Override
    public StructureRS getAll(BaseListingRQ request) {
        Page<CategoryEntityInfo> categoryEntityInfos = categoryRepository.findByQuery(request.getQuery(), request.getPageable(request.getSort(),request.getOrder()));
        List<CategoryRS> categoryRSList = new ArrayList<>();
        if (categoryEntityInfos.isEmpty())
            return response(categoryRSList);

        List<CategoryItemRS> parentList = categoryEntityInfos.stream().map(it -> {
            CategoryItemRS item = new CategoryItemRS();
            item.setId(it.getParent().getId());
            item.setName(it.getParent().getName());
            item.setUuid(it.getUuid());

            return item;
        }).distinct().toList();

        for (CategoryItemRS itemRS : parentList) {
            CategoryRS parent = new CategoryRS();
            parent.setCategory(itemRS);

            List<CategoryEntityInfo> children = categoryEntityInfos.stream().filter(it-> it.getParent().getId().equals(itemRS.getId())).toList();
            List<CategoryItemRS> childList = children.stream().map(it-> new CategoryItemRS(it.getId(), it.getName(),it.getUuid())).toList();
            parent.setItems(childList);

            categoryRSList.add(parent);
        }

        return response(categoryRSList, categoryEntityInfos);
    }

}
