package com.example.demo.service.OrderStatus;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.OrderStatusEntity;
import com.example.demo.db.repository.OrderStatusRepository;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.OrderStatus.OrderStatusEntityInfo;
import com.example.demo.model.request.OrderStatus.OrderStatusRQ;
import com.example.demo.model.request.OrderStatus.UpdateOrderStatusRQ;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
@AllArgsConstructor
public class OrderStatusServiceImpli extends BaseService implements OrderStatusService {
    private final OrderStatusRepository orderStatusRepository;
    @Override
    public StructureRS getOrderStatus(BaseListingRQ request) {
        Page<OrderStatusEntityInfo>orderStatusEntityInfos=orderStatusRepository.searchStatus(request.getQuery(),request.getPageable(request.getSort()));
        return response(orderStatusEntityInfos.getContent(),orderStatusEntityInfos);
    }

    @Override
    public void createOrderStatus(OrderStatusRQ request) {
        boolean isExist=orderStatusRepository.existsByStatus(request.getStatus());
        if(isExist)
            throw new InternalServerError(MessageConstant.ORDERSTATUS.ORDER_EXIST);

        OrderStatusEntity newOrderStatus=new OrderStatusEntity();
        newOrderStatus.setStatus(request.getStatus());
        orderStatusRepository.save(newOrderStatus);
    }

    @Override
    public void updateOrderStatus(Long id, UpdateOrderStatusRQ request) {
      OrderStatusEntity findOrderID=orderStatusRepository.findById(id).stream().findFirst()
              .orElseThrow(()->new NotFoundException(MessageConstant.ORDERSTATUS.ORDER_FOUND));
      findOrderID.setStatus(request.getStatus());
      orderStatusRepository.save(findOrderID);
    }

    @Override
    public void deleteOrderStatus(Long id) {
        OrderStatusEntity findOrderID=orderStatusRepository.findById(id).stream().findFirst()
                .orElseThrow(()->new NotFoundException(MessageConstant.ORDERSTATUS.ORDER_FOUND));
        boolean isDelete = orderStatusRepository.existsByDeleted();
        if (!isDelete){
            findOrderID.setDeletedAt(Instant.now());
            orderStatusRepository.save(findOrderID);
        }else {
            throw new InternalServerError(MessageConstant.ORDERSTATUS.ORDER_ALREADY_DELETED);
        }
    }
}
