package com.example.demo.service.variation;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.entity.VariationEntity;

public interface VariationService {

    /**
     * Get all Variation item
     * @param request is request body of Variation item
     * @return response
     */
    StructureRS getAllVariations(BaseListingRQ request);

    /**
     * This method is used to find Variation by id
     * @param id of Variation (unique identifier)
     * @param request is the request data from client
     * @return response
     */
    StructureRS getVariationById(Long id , BaseListingRQ request);

    /**
     * Add Variation item
     * @param request is request body of Variation item
     * default value of status add items is true (active)
     */
    StructureRS createVariation(VariationEntity request);

    /**
     * Update Variation item
     * @param id is id of Variation item
     * @param request is request body of Variation item
     */
    StructureRS updateVariation(Long id, VariationEntity request);

    /**
     * Get Variation item
     * @param id is id of Variation item
     * @return Variation item response
     * Note : Status true is not delete
     */
    StructureRS deleteVariation (Long id);
}
