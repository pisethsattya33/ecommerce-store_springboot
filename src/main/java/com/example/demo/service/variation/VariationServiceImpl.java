package com.example.demo.service.variation;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.VariationEntity;
import com.example.demo.db.repository.CategoryRepository;
import com.example.demo.db.repository.VariationRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.variation.VariationEntityInfo;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
@Slf4j
public class VariationServiceImpl extends BaseService implements VariationService {

    private final VariationRepository variationRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public StructureRS getAllVariations(BaseListingRQ request) {
        Page<VariationEntityInfo> variationEntities = variationRepository.findByName(request.getQuery(), request.getPageable(request.getSort(), request.getOrder()));
        return response(variationEntities.getContent(), variationEntities);
    }

    @Override
    public StructureRS getVariationById(Long id, BaseListingRQ request) {
        Page<VariationEntityInfo> variationEntityInfos = variationRepository.findByIdEqualsAndDeletedAtIsNull(id, request.getPageable());
        if (variationEntityInfos.isEmpty())
            throw new BadRequestException(MessageConstant.VARIATION.VARIATION_NOT_FOUND);
        return response(variationEntityInfos.getContent(), variationEntityInfos);
    }

    @Override
    public StructureRS createVariation(VariationEntity request) {
        if (variationRepository.existsByName(request.getName())) {
            throw new BadRequestException(MessageConstant.VARIATION.VARIATION_EXIST);
        }

        if (!categoryRepository.existsById(request.getCategory().getId())) {
            throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
        }
        VariationEntity newVariation = new VariationEntity();
        newVariation.setName(request.getName());
        newVariation.setCategory(request.getCategory());
        variationRepository.save(newVariation);
        return response(MessageConstant.VARIATION.VARIATION_CREATED_SUCCESSFULLY);
    }

    @Transactional
    @Override
    public StructureRS updateVariation(Long id, VariationEntity request) {
        VariationEntity existingVariation = variationRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION.VARIATION_NOT_FOUND));
        if (!categoryRepository.existsById(request.getCategory().getId())) {
            throw new NotFoundException(MessageConstant.CATEGORY.CATEGORY_NOT_FOUND);
        }
        if (variationRepository.existsByNameAndIdNot(request.getName(), id)) {
            throw new InternalServerError(MessageConstant.VARIATION.VARIATION_EXIST);
        }
        existingVariation.setName(request.getName());
        existingVariation.setCategory(request.getCategory());
        variationRepository.save(existingVariation);
        return response(MessageConstant.VARIATION.VARIATION_UPDATED_SUCCESSFULLY);
    }

    @Override
    public StructureRS deleteVariation(Long id) {
        VariationEntity variationEntity = variationRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION.VARIATION_NOT_FOUND));
        variationEntity.setDeletedAt(Instant.now());
        variationRepository.save(variationEntity);
        return response();
    }
}
