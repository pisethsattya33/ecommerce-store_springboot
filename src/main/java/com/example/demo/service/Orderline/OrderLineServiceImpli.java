package com.example.demo.service.Orderline;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.OrderLineEntity;
import com.example.demo.db.entity.ProductItemEntity;
import com.example.demo.db.entity.ShippingMethodEntity;
import com.example.demo.db.entity.ShopOrderEntity;
import com.example.demo.db.repository.OrderLineRepository;
import com.example.demo.db.repository.ProductItemRepository;
import com.example.demo.db.repository.ShopOrderRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.Orderline.OrderLineEntityInfo;
import com.example.demo.model.request.Orderline.OrderlineRQ;
import com.example.demo.model.request.Orderline.ProductItemRQ;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
public class OrderLineServiceImpli extends BaseService implements OrderLineService {

    private final OrderLineRepository orderLineRepository;
    private final ProductItemRepository productItemRepository;
    private final ShopOrderRepository shopOrderRepository;

    @Override
    public StructureRS getALlOrderline(Long order_id, BaseListingRQ request) {
        Page<OrderLineEntityInfo> order = orderLineRepository.findByIdAndDeletedAtNull(order_id, request.getQuery(),request.getPageable(request.getSort(), request.getOrder()));
        return response(order.getContent(), order);
    }

@Override
@Transactional
public void createOrderline(OrderlineRQ request) {
    Long orderId = request.getOrder_id().stream().findFirst().orElseThrow(() -> new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERLINEID));
    ShopOrderEntity order = shopOrderRepository.findById(orderId)
            .orElseThrow(() -> new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERLINEID));

    List<ProductItemRQ> orderlineItems = request.getOrderline();
    for (ProductItemRQ item : orderlineItems) {
        Set<Long> productItemIds = item.getProduct_item_id();
        for (Long productItemId : productItemIds) {
            ProductItemEntity productItem = productItemRepository.findById(productItemId)
                    .orElseThrow(() -> new NotFoundException(MessageConstant.ORDERLINE.CANNOTFINDPRODUCTITEMID));

            OrderLineEntity orderLine = new OrderLineEntity();
            orderLine.setProductItem(productItem);
            orderLine.setShopOrder(order);
            orderLine.setQuantity(item.getQty());
            orderLine.setPrice(productItem.getPrice());
            orderLineRepository.save(orderLine);

            int remainingQuantity = productItem.getQuantity() - item.getQty();
            if (remainingQuantity < 0) {
                throw new BadRequestException("Insufficient quantity for product item " + productItem.getId());
            } else {
                productItem.setQuantity(remainingQuantity);
                productItemRepository.save(productItem);
            }
        }
    }
    // Calculate total order amount for all order lines
    BigDecimal totalOrderline = calculateTotalOrderAmount(order.getOrderLines());
    order.setOrderTotal(totalOrderline);
    BigDecimal totalShipping = calculateTotalTaxShipping(order.getShippingMethod());

    // Update order total including shipping cost
    BigDecimal totalOrderWithShipping = totalOrderline.add(totalShipping);
    order.setOrderTotal(totalOrderWithShipping);
}

    private BigDecimal calculateTotalOrderAmount(List<OrderLineEntity> orderLines) {
        BigDecimal totalOrderline = BigDecimal.ZERO;

        for (OrderLineEntity orderLine : orderLines) {
            BigDecimal lineTotal = orderLine.getPrice().multiply(BigDecimal.valueOf(orderLine.getQuantity()));
            totalOrderline = totalOrderline.add(lineTotal);
        }
        return totalOrderline;
    }
    private BigDecimal calculateTotalTaxShipping(ShippingMethodEntity shippingMethod){
        return shippingMethod.getPrice();
    }


    @Override
    @Transactional
    public void deleteOrderLine(Long id) {
    boolean isdeleted=orderLineRepository.existsByDeleted(id);
    OrderLineEntity orderLine =orderLineRepository.findById(id).orElseThrow(()->new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERID));
    if(isdeleted)
        throw new InternalServerError(MessageConstant.ORDERLINE.CANNOTDELETEDTWEICE);

    if (orderLine == null){
        throw new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERID);
    }else {
        ProductItemEntity productItem = orderLine.getProductItem();
        int currentQuantity = productItem.getQuantity();
        int quantityToAddBack = orderLine.getQuantity();
        productItem.setQuantity(currentQuantity + quantityToAddBack);
        productItemRepository.save(productItem);

        orderLine.setDeletedAt(Instant.now());
        orderLineRepository.save(orderLine);
    }
    }

}
