package com.example.demo.service.ShoppingCart;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.ProductItemEntity;
import com.example.demo.db.entity.ShoppingCartEntity;
import com.example.demo.db.entity.ShoppingCartItemEntity;
import com.example.demo.db.entity.UserEntity;
import com.example.demo.db.repository.ProductItemRepository;
import com.example.demo.db.repository.ShoppingCartItemRepository;
import com.example.demo.db.repository.ShoppingCartRepository;
import com.example.demo.db.repository.UserRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.ShoppingCart.ShoppingCartEntityInfo;
import com.example.demo.model.request.ShoppingCart.ShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateShoppingCartRQ;
import com.example.demo.model.request.ShoppingCart.UpdateStatusShoppingCartRQ;
import com.example.demo.security.UserPrincipal;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ShoppingCartServiceImpli extends BaseService implements ShoppingCartService {

    private final ShoppingCartRepository shoppingCartRepository;
    private final UserRepository userRepository;
    private final ProductItemRepository productItemRepository;
    private final ShoppingCartItemRepository shoppingCartItemRepository;

    @Override
    public StructureRS getShoppingCart(UserPrincipal userPrincipal, BaseListingRQ request) {
        Page<ShoppingCartEntityInfo> shoppingCartEntityInfos = shoppingCartRepository.findByUser_IdAndDeletedAtNull(userPrincipal.getId(), request.getPageable(request.getSort(), request.getOrder()));
        return response(shoppingCartEntityInfos.getContent(), shoppingCartEntityInfos);

    }

    @Override
    @Transactional
    public void createShoppingCart(ShoppingCartRQ request) {
        UserEntity userEntity = userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(() ->
                new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));
        ShoppingCartEntity findUserInShopcart = shoppingCartRepository.findByUser(userEntity);
        if (findUserInShopcart == null) {
            findUserInShopcart = new ShoppingCartEntity();
            findUserInShopcart.setUser(userEntity);
        }
        shoppingCartRepository.save(findUserInShopcart);
        ShoppingCartItemEntity shoppingCartItemEntity = new ShoppingCartItemEntity();
        ShoppingCartEntity savedShoppingCartEntity = shoppingCartRepository.findById(findUserInShopcart.getId()).orElseThrow(() ->
                new NotFoundException(MessageConstant.SHOPPINGCART.SHOPPING_CART_NOT_FOUND));
        Long productItemId = request.getProduct_item_id();
// Retrieve the product item
        ProductItemEntity productItemID = productItemRepository.findById(productItemId)
                .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPINGCART.PRODUCTNOTFOUND));
// Check if the product item already exists in the shopping cart
        ShoppingCartItemEntity existingItem = shoppingCartItemRepository.findByShoppingCartAndProductItem(findUserInShopcart, productItemID);

        if (productItemID.getQuantity() <= 0) {
            throw new NotFoundException(MessageConstant.PRODUCT_ITEM.PRODUCT_ITEM_OUT_OF_STOCK);
        } else if (productItemID.getQuantity() < request.getQty()) {
            throw new NotFoundException(MessageConstant.PRODUCT_ITEM.PRODUCT_ITEM_NOT_ENOUGH);
        } else {
            if (existingItem != null) {
                existingItem.setQuantity(existingItem.getQuantity() + request.getQty());
                shoppingCartItemRepository.save(existingItem);
            } else {
                shoppingCartItemEntity.setProductItem(productItemID);
                shoppingCartItemEntity.setShoppingCart(savedShoppingCartEntity);
                shoppingCartItemEntity.setQuantity(request.getQty());
                shoppingCartItemRepository.save(shoppingCartItemEntity);
            }
        }


    }


    @Override
    @Transactional
    public void updateShoppingCart(UserPrincipal userPrincipal, UpdateShoppingCartRQ request) {
        ShoppingCartItemEntity updateShoppingcart = shoppingCartItemRepository.findById(userPrincipal.getId()).stream().findFirst().orElseThrow(() -> new NotFoundException(""));
        Long product_item = request.getProduct_item_id();
        Optional<ProductItemEntity> productItemID = productItemRepository.findById(product_item);

        if (updateShoppingcart != null) {
            updateShoppingcart.setProductItem(productItemID.orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPINGCART.PRODUCTNOTFOUND)));
            updateShoppingcart.setQuantity(request.getQty());
        } else {
            throw new NotFoundException(MessageConstant.SHOPPINGCART.SHOPCARTIDNOTFOUND);
        }
    }

    @Override
    public void updateShoppingCartStatus(UserPrincipal userPrincipal, UpdateStatusShoppingCartRQ[] request) {

        ShoppingCartEntity shoppingCart = shoppingCartRepository.findByUser_Id(userPrincipal.getId());
//                .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPINGCART.SHOPCARTIDNOTFOUND));

        for (UpdateStatusShoppingCartRQ requestItem : request) {
            ShoppingCartItemEntity shoppingCartItem = shoppingCartItemRepository.findById(requestItem.getItemid())
                    .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPINGCART.SHOPCARTIDNOTFOUND));
            if (requestItem.getStatus()) {
                shoppingCartItem.setDeletedAt(Instant.now());
            } else {
                shoppingCartItem.setDeletedAt(null);
            }
            shoppingCartItemRepository.save(shoppingCartItem);
        }

        // Check if all items in the cart are deleted
        Long shop_id = shoppingCart.getId();
        List<ShoppingCartItemEntity> shoppingCartItems = shoppingCartItemRepository.findByShoppingCartId(shop_id);
        boolean allItemsDeleted = shoppingCartItems.stream().allMatch(item -> item.getDeletedAt() != null);
        shoppingCart.setDeletedAt(allItemsDeleted ? Instant.now() : null);
        shoppingCartRepository.save(shoppingCart);

    }
}
