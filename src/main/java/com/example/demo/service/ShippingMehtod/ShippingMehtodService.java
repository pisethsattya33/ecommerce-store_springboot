package com.example.demo.service.ShippingMehtod;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.ShippingMethod.ShippingMethodRQ;
import com.example.demo.model.request.ShippingMethod.UpdateShippingMethodRQ;

public interface ShippingMehtodService {
    StructureRS getShippingMethod(BaseListingRQ request);
    void createShippingMethod(ShippingMethodRQ request);
    void updateShippingMethod(Long id,UpdateShippingMethodRQ request);
    void deleteShippingMethod(Long id);
}
