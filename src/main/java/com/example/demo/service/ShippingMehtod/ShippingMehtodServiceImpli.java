package com.example.demo.service.ShippingMehtod;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.ShippingMethodEntity;
import com.example.demo.db.repository.ShippingMethodRepository;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.ShippingMethod.ShippingMethodEntityInfo;
import com.example.demo.model.request.ShippingMethod.ShippingMethodRQ;
import com.example.demo.model.request.ShippingMethod.UpdateShippingMethodRQ;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;


@AllArgsConstructor
@Service
public class ShippingMehtodServiceImpli extends BaseService implements ShippingMehtodService {
    private final ShippingMethodRepository shippingMethodRepository;
    @Override
    public StructureRS getShippingMethod(BaseListingRQ request) {
        Page<ShippingMethodEntityInfo>shippingMethodEntityInfos=shippingMethodRepository.findByName(request.getQuery(),request.getPageable(request.getSort()));
        return response(shippingMethodEntityInfos.getContent(),shippingMethodEntityInfos);
    }

    @Override
    public void createShippingMethod(ShippingMethodRQ request) {
        Boolean isNameExist =shippingMethodRepository.existsByName(request.getName());
        if (!isNameExist){
            ShippingMethodEntity newShippingMethod=new ShippingMethodEntity();
            newShippingMethod.setName(request.getName());
            newShippingMethod.setPrice(request.getPrice());
            shippingMethodRepository.save(newShippingMethod);
        }else {
            throw new InternalServerError(MessageConstant.SHIPPING_METHOD.SHIPPING_ALREADY_EXIST);
        }
    }

    @Override
    public void updateShippingMethod(Long id, UpdateShippingMethodRQ request) {
    ShippingMethodEntity shippingMethod =shippingMethodRepository.findById(id).orElseThrow(()-> new NotFoundException(MessageConstant.SHIPPING_METHOD.SHIPPING_ID_NOT_FOUND));
    shippingMethod.setName(request.getName());
    shippingMethod.setPrice(request.getPrice());
    shippingMethodRepository.save(shippingMethod);
    }

    @Override
    public void deleteShippingMethod(Long id) {
        ShippingMethodEntity findShippingID=shippingMethodRepository.findById(id)
                .orElseThrow(()->new NotFoundException(MessageConstant.SHIPPING_METHOD.SHIPPING_ID_NOT_FOUND));
        boolean isDeleted = shippingMethodRepository.existsByIdAndDeleted(id);
        if (!isDeleted){
            findShippingID.setDeletedAt(Instant.now());
            shippingMethodRepository.save(findShippingID);
        }else {
            throw new InternalServerError(MessageConstant.SHIPPING_METHOD.SHIPPING_MEHTOD_ALREADY_DELETED);
        }
    }
}
