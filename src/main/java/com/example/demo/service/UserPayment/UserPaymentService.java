package com.example.demo.service.UserPayment;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.status.Provider;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentRQ;
import com.example.demo.security.UserPrincipal;

public interface UserPaymentService {
    /**
     * Get all user payment and searching
     */
    StructureRS getPayment(BaseListingRQ request, Provider provider);
    /**
     *
     * @param request for body of user payment
     */
    void Create(UserPaymentRQ request,UserPrincipal userPrincipal);

    /**
     *
     * @param id for user payment when it needs to update
     * @param request body of user payment
     */
    void Update(Long id, UserPaymentRQ request);

    /**
     *
     * @param id for user payment when it needs to delete
     */
    void Delete(Long id);

    StructureRS getPaymentToken(UserPrincipal principal);

    void updateStatusDefault(Long id);
}
