package com.example.demo.service.ShoppingOrder;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.ShoppingOrder.ShoppingCartOrderRQ;
import com.example.demo.model.request.ShoppingOrder.ShoppingOrderRQ;
import com.example.demo.model.request.ShoppingOrder.UpdateShoppingOrderRQ;
import com.example.demo.model.request.ShoppingOrder.UpdateShoppingOrderStatus;
import com.example.demo.security.UserPrincipal;

public interface ShoppingOrderService {
    /**
     * for search shop order
     */
    StructureRS getShoppingOrder(Long order_id,BaseListingRQ request);
    StructureRS getShoppingOrderByUser(UserPrincipal userPrincipal,BaseListingRQ request);

    /**
     *
     * @param request body for shop order
     */
    void createShoppingOrder(ShoppingOrderRQ request);
    void createShoppingCartOrder(UserPrincipal userPrincipal,ShoppingCartOrderRQ request);

    /**
     *
     * @param id for shop order update
     * @param request body for shop order
     */
    void updateShoppingOrder(Long id, UpdateShoppingOrderRQ request);

    /**
     *
     * @param id for shop order update
     * @param request status processing and Delivered
     */
    void updateShoppingOrderStatus(Long id,UpdateShoppingOrderStatus request);

    /**
     *
     * @param id for delete Shop order
     */
    void deleteShoppingOrder(Long id);
}
