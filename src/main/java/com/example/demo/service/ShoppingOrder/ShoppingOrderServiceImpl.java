package com.example.demo.service.ShoppingOrder;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.*;
import com.example.demo.db.repository.*;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.ShopOrder.ShopOrderEntityInfo;
import com.example.demo.model.request.ShoppingOrder.*;
import com.example.demo.security.UserPrincipal;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.*;

@Service
@AllArgsConstructor

public class ShoppingOrderServiceImpl extends BaseService implements ShoppingOrderService {
    private final ShippingMethodRepository shippingMethodEntityRepository;
    private final OrderStatusRepository orderStatusRepository;
    private final ShopOrderRepository shopOrderRepository;
    private final OrderLineRepository orderLineRepository;
    private final ProductItemRepository productItemRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final ShoppingCartItemRepository shoppingCartItemRepository;
    private final UserRepository userRepository;
    private final UserPaymentMethodRepository userPaymentMethodRepository;

    @Override
    public StructureRS getShoppingOrder(Long order_id,BaseListingRQ request) {
        Page<ShopOrderEntityInfo>shopOrder=shopOrderRepository.findByOrderLines_Id(order_id,request.getQuery(),request.getPageable(request.getSort(),request.getOrder()));
        return response(shopOrder.getContent(),shopOrder);
    }

    @Override
    public StructureRS getShoppingOrderByUser(UserPrincipal userPrincipal, BaseListingRQ request) {
        Page<ShopOrderEntityInfo>shopOrderEntity=shopOrderRepository.findByUser_IdAndDeletedAtNull(userPrincipal.getId(),request.getPageable(request.getSort()));
        return response(shopOrderEntity.getContent(),shopOrderEntity);
    }

    @Override
        @Transactional
        public void createShoppingOrder(ShoppingOrderRQ request) {
            BigDecimal totalOrderline = BigDecimal.ZERO;

            ShopOrderEntity shopOrder = new ShopOrderEntity();
            UserEntity user = userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(()->new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));

            Long methodID = request.getShippingMethod();
            Optional<ShippingMethodEntity> shippingMethod = shippingMethodEntityRepository.findById(methodID);
            Long payid =request.getUser_payment_method();

           UserPaymentMethodEntity userPaymentMethod=userPaymentMethodRepository.findByIdAndIsDefaultTrue(payid);

//           if (userPaymentMethod == null || userPaymentMethod.getIsDefault()){
//               throw new NotFoundException("Make sure payment is active");
//           }

            Optional<OrderStatusEntity> orderStatus = orderStatusRepository.findById(1L);
            shopOrder.setUser(user);
            shopOrder.setOrderDate(Date.from(Instant.now()));
            shopOrder.setShippingMethod(shippingMethod.orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.SHIPPING_MEHTOD_NOT_FOUND_ID)));
            shopOrder.setOrderStatus(orderStatus.orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.ORDERSTATUSNOTFOUND)));
            shopOrder.setShippingAddress(request.getShippingAddress());
            shopOrder.setUserPaymentMethods(Collections.singletonList(userPaymentMethod));
            shopOrder.setOrderTotal(BigDecimal.valueOf(0.00));

            shopOrderRepository.save(shopOrder);

            ShopOrderEntity shopOrderEntity=shopOrderRepository.findById(shopOrder.getId()) .orElseThrow(() -> new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERLINEID));
//            UserPaymentMethodEntity userPaymentMethod=userPaymentMethodRepository.findByUser_IdAndIsDefaultTrue(user.getId());
//
//            if (userPaymentMethod == null){
//                throw new NotFoundException("cannot find payment");
//            }else {
//                userPaymentMethod.setShopOrder(shopOrderEntity);
//                userPaymentMethodRepository.save(userPaymentMethod);
//            }

            shopOrderEntity.setId(shopOrderEntity.getId());

            for (ProductitemsRQ items :request.getOrderlines()){

              Long productItemIds = items.getProduct_item_id();
//                for (Long productItemId : productItemIds){
                    ProductItemEntity productItem=productItemRepository.findById(productItemIds).orElseThrow(()->new NotFoundException("no product id"+items));
                    OrderLineEntity orderLine = new OrderLineEntity();
                    orderLine.setShopOrder(shopOrderEntity);
                    orderLine.setProductItem(productItem);
                    orderLine.setQuantity(items.getQty());
                    orderLine.setPrice(productItem.getPrice());
                    BigDecimal lineTotal = productItem.getPrice().multiply(BigDecimal.valueOf(items.getQty()));
                    totalOrderline = totalOrderline.add(lineTotal);

                    orderLineRepository.save(orderLine);

                    int remainingQuantity = productItem.getQuantity() - items.getQty();
                    if (remainingQuantity < 0) {
                        throw new BadRequestException(MessageConstant.PRODUCT_ITEM.PRODUCT_ITEM_RAN_OUT + productItem.getId());
                    } else {
                        productItem.setQuantity(remainingQuantity);
                        productItemRepository.save(productItem);
                    }

//                }

            }
    //         Calculate total order amount for all order lines
            BigDecimal totalShipping = calculateTotalTaxShipping(shopOrder.getShippingMethod());
            BigDecimal totalOrderWithShipping = totalOrderline.add(totalShipping);
            shopOrder.setOrderTotal(totalOrderWithShipping);
            // Save the shop order
//            shopOrderRepository.save(shopOrder);
        }


    private BigDecimal calculateTotalTaxShipping(ShippingMethodEntity shippingMethod){
        return shippingMethod.getPrice();
    }
    @Override
    public void createShoppingCartOrder(UserPrincipal userPrincipal, ShoppingCartOrderRQ request) {
        BigDecimal totalOrderline = BigDecimal.ZERO;

        UserEntity user = userRepository.findByUsernameAndIsDeletedFalseAndStatusTrue(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(()->new BadRequestException(MessageConstant.AUTH.INVALID_TOKEN));
        Long methodID = request.getShippingMethod();
        ShippingMethodEntity shippingMethod = shippingMethodEntityRepository.findById(methodID)
                .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.SHIPPING_MEHTOD_NOT_FOUND_ID));
        OrderStatusEntity orderStatus = orderStatusRepository.findById(1L)
                .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.ORDERSTATUSNOTFOUND));
//        UserPaymentMethodEntity userPaymentMethod=userPaymentMethodRepository.findByUser_IdAndIsDefaultTrue(user.getId());
        Long payid =request.getUser_payment_method();
        UserPaymentMethodEntity userPaymentMethod=userPaymentMethodRepository.getReferenceById(payid);

        ShoppingCartEntity Cart = shoppingCartRepository.findByUser(userPrincipal.getId());
        if (Cart==null){
           throw new NotFoundException(MessageConstant.SHOPPINGCART.SHOPPING_CART_NOT_FOUND);
        }
//        if (userPaymentMethod==null){
//            throw new NotFoundException("cannot find payment");
//        }else {

        ShopOrderEntity shopOrder = new ShopOrderEntity();
        shopOrder.setUser(user);
        shopOrder.setOrderDate(Date.from(Instant.now()));
        shopOrder.setShippingMethod(shippingMethod);
        shopOrder.setOrderStatus(orderStatus);
        shopOrder.setUserPaymentMethods(Collections.singletonList(userPaymentMethod));
        shopOrder.setShippingAddress(request.getShippingAddress());
        shopOrder.setOrderTotal(BigDecimal.ZERO);

        shopOrderRepository.save(shopOrder);
        // Fetch the saved shop order to ensure it has an ID
//        shopOrder = shopOrderRepository.findById(shopOrder.getId())
//                .orElseThrow(() -> new NotFoundException(MessageConstant.ORDERLINE.NOTFOUNDORDERLINEID));
        // Associate payment method with the shop order

//            userPaymentMethod.setShopOrder(shopOrder);
//            userPaymentMethodRepository.save(userPaymentMethod);



        for (ShoppingCartItemEntity cartItem : Cart.getShoppingCartItems()) {
            OrderLineEntity orderLine = new OrderLineEntity();
            orderLine.setProductItem(cartItem.getProductItem());
            orderLine.setQuantity(cartItem.getQuantity());
            orderLine.setPrice(cartItem.getProductItem().getPrice());
            orderLine.setShopOrder(shopOrder);
            orderLineRepository.save(orderLine);
            BigDecimal lineTotal = cartItem.getProductItem().getPrice().multiply(BigDecimal.valueOf(cartItem.getQuantity()));
            totalOrderline = totalOrderline.add(lineTotal);

//            shopOrder.setOrderTotal(shopOrder.getOrderTotal()
//                    .add(cartItem.getProductItem().getPrice().multiply(BigDecimal.valueOf(cartItem.getQuantity()))));

            BigDecimal totalShipping = calculateTotalTaxShipping(shopOrder.getShippingMethod());
            BigDecimal totalOrderWithShipping = totalOrderline.add(totalShipping);
            shopOrder.setOrderTotal(totalOrderWithShipping);
            shopOrderRepository.save(shopOrder);

            ShoppingCartEntity shoppingCart = shoppingCartRepository.findByUser_Id(userPrincipal.getId());
            if (shoppingCart != null) {
                List<ShoppingCartItemEntity> cartItems = shoppingCartItemRepository.findByShoppingCartId(shoppingCart.getId());

                for (ShoppingCartItemEntity removeCartItem : cartItems) {
                    shoppingCartItemRepository.delete(removeCartItem);
                }

                shoppingCartRepository.delete(shoppingCart);
            }
        }
        }

    @Override
    @Transactional
    public void updateShoppingOrder(Long id, UpdateShoppingOrderRQ request) {
        ShopOrderEntity shopOrderID = shopOrderRepository.findById(id).orElseThrow(()->new NotFoundException(MessageConstant.SHOPPING_OREDER.SHOPORDERIDNOTFOUND));

            Long methodID = request.getShippingMethod();
            Optional<ShippingMethodEntity> shippingMethod = shippingMethodEntityRepository.findById(methodID);
            shopOrderID.setShippingMethod(shippingMethod.orElseThrow());
            shopOrderID.setShippingAddress(request.getShippingAddress());
            shopOrderRepository.save(shopOrderID);
    }

    @Override
    @Transactional
    public void updateShoppingOrderStatus(Long id, UpdateShoppingOrderStatus request) {
        ShopOrderEntity findOrderstatus = shopOrderRepository.findByIdAndDeletedAtNull(id);
        if (findOrderstatus != null) {
            Long orderID = (request.getOrderstatus());
            Optional<OrderStatusEntity> defaultOrderStatus = orderStatusRepository.findById(orderID);
            OrderStatusEntity orderStatus = defaultOrderStatus.orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.DEFAULT_ORDER_STATUS_NOT_FOUND));
            findOrderstatus.setOrderStatus(orderStatus);
            shopOrderRepository.save(findOrderstatus);
        } else {
            throw new NotFoundException(MessageConstant.SHOPPING_OREDER.SHOPORDERIDNOTFOUND);
        }
    }

@Override
@Transactional
public void deleteShoppingOrder(Long id) {

    ShopOrderEntity shopOrder = shopOrderRepository.findById(id)
            .orElseThrow(() -> new NotFoundException(MessageConstant.SHOPPING_OREDER.SHOPORDERIDNOTFOUND));

    if (shopOrder.getDeletedAt() != null) {
        throw new InternalServerError(MessageConstant.SHOPPING_OREDER.SHOPPINGORDERALREADDELETED);
    }
    for (OrderLineEntity orderLine : shopOrder.getOrderLines()) {
        ProductItemEntity productItem = orderLine.getProductItem();
        int currentQuantity = productItem.getQuantity();
        int quantityToAddBack = orderLine.getQuantity();
        productItem.setQuantity(currentQuantity + quantityToAddBack);
        productItemRepository.save(productItem);

        orderLine.setDeletedAt(Instant.now());
        orderLineRepository.save(orderLine);
    }
    shopOrder.setDeletedAt(Instant.now());
    shopOrderRepository.save(shopOrder);
}
}
