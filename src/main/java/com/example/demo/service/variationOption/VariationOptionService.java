package com.example.demo.service.variationOption;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.entity.VariationEntity;
import com.example.demo.db.entity.VariationOptionEntity;
import com.example.demo.model.request.variation.CreateVariationOptionRQ;

public interface VariationOptionService {

    /**
     * Get all VariationOption item
     * @param request is request body of VariationOption item
     * @return response
     */
    StructureRS getAllVariationsOption(BaseListingRQ request);

    /**
     * This method is used to find VariationOption by id
     * @param id of VariationOption (unique identifier)
     * @param request is the request data from client
     * @return response
     */
    StructureRS getVariationOptionById(Long id , BaseListingRQ request);

    /**
     * Add VariationOption item
     * @param request is request body of VariationOption item
     * default value of status add items is true (active)
     */
    StructureRS createVariationOption(CreateVariationOptionRQ request);

    /**
     * Update VariationOption item
     * @param id is id of VariationOption item
     * @param request is request body of VariationOption item
     */
    StructureRS updateVariationOption(Long id, CreateVariationOptionRQ request);

    /**
     * Get VariationOption item
     * @param id is id of VariationOption item
     * @return VariationOption item response
     * Note : Status true is not delete
     */
    void deleteVariationOption(Long id);


}
