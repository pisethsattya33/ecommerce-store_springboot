package com.example.demo.service.variationOption;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.VariationEntity;
import com.example.demo.db.entity.VariationOptionEntity;
import com.example.demo.db.repository.CategoryRepository;
import com.example.demo.db.repository.VariationOptionRepository;
import com.example.demo.db.repository.VariationRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.variation.VariationEntityInfo;
import com.example.demo.model.projection.variationOption.VariationOptionEntityInfo;
import com.example.demo.model.request.variation.CreateVariationOptionRQ;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class VariationOptionServiceImpl extends BaseService implements VariationOptionService {

    private final VariationOptionRepository variationOptionRepository;
    private final VariationRepository variationRepository;

    @Override
    public StructureRS getAllVariationsOption(BaseListingRQ request) {
        Page<VariationOptionEntityInfo> variationOptionEntityInfos = variationOptionRepository.findByValue(request.getQuery(), request.getPageable(request.getSort(),request.getOrder()));
        return response(variationOptionEntityInfos.getContent(), variationOptionEntityInfos);
    }

    @Override
    public StructureRS getVariationOptionById(Long id, BaseListingRQ request) {
        Page<VariationOptionEntityInfo> variationOptionEntityInfos = variationOptionRepository.findByIdEqualsAndDeletedAtIsNull(id, request.getPageable());
        if (variationOptionEntityInfos.isEmpty())
            throw new NotFoundException(MessageConstant.VARIATION.VARIATION_NOT_FOUND);
        return response(variationOptionEntityInfos.getContent(), variationOptionEntityInfos);
    }

    @Override
    public StructureRS createVariationOption(CreateVariationOptionRQ request) {
        if (variationOptionRepository.existsByValue(request.getValue())) {
            throw new InternalServerError(MessageConstant.VARIATION_OPTION.VARIATION_OPTION_EXIST);
        }

        VariationEntity variationEntity = variationRepository.findById(request.getVariationId())
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION.VARIATION_NOT_FOUND));

        VariationOptionEntity variationOptionEntity = new VariationOptionEntity();
        variationOptionEntity.setValue(request.getValue());
        variationOptionEntity.setVariation(variationEntity);

        variationOptionRepository.save(variationOptionEntity);

        return response(MessageConstant.VARIATION_OPTION.VARIATION_OPTION_CREATED_SUCCESSFULLY);
    }


    @Override
    public StructureRS updateVariationOption(Long id ,CreateVariationOptionRQ request) {
        VariationOptionEntity variationOptionEntity = variationOptionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION_OPTION.VARIATION_OPTION_NOT_FOUND));

        VariationEntity newVariation = variationRepository.findById(request.getVariationId())
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION.VARIATION_NOT_FOUND));

        variationOptionEntity.setValue(request.getValue());
        variationOptionEntity.setVariation(newVariation);

        variationOptionRepository.save(variationOptionEntity);

        return response(MessageConstant.VARIATION_OPTION.VARIATION_OPTION_UPDATED_SUCCESSFULLY);
    }


    @Override
    public void deleteVariationOption(Long id) {
        VariationOptionEntity variationOptionEntity = variationOptionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(MessageConstant.VARIATION_OPTION.VARIATION_OPTION_NOT_FOUND));
        variationOptionEntity.setDeletedAt(null);
            variationOptionEntity.setDeletedAt(Instant.now());
        variationOptionRepository.save(variationOptionEntity);
        }

    }
