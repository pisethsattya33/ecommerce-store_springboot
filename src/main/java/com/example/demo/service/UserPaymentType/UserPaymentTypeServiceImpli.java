package com.example.demo.service.UserPaymentType;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.BaseService;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.PaymentTypeEntity;
import com.example.demo.db.repository.PaymentTypeRepository;
import com.example.demo.exception.httpstatus.BadRequestException;
import com.example.demo.exception.httpstatus.InternalServerError;
import com.example.demo.exception.httpstatus.NotFoundException;
import com.example.demo.model.projection.UserPayment.PaymentTypeEntityInfo;
import com.example.demo.model.request.UserPaymentRQ.UserPaymentTypeRQ;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class UserPaymentTypeServiceImpli extends BaseService implements UserPaymentTypeService {
   private final PaymentTypeRepository paymentTypeRepository;

    @Override
    public StructureRS getPaymentType(BaseListingRQ request) {
        Page<PaymentTypeEntityInfo>paymentTypeEntityInfoPage = paymentTypeRepository.findByValueAndDeletedAtNull(request.getQuery(),request.getPageable(request.getSort(),request.getOrder()));
        return response(paymentTypeEntityInfoPage.getContent(),paymentTypeEntityInfoPage);
    }

    @Override
    @Transactional
    public void createPaymentType(UserPaymentTypeRQ request) {
        boolean isexists = paymentTypeRepository.existValue((request.getValue()));
        PaymentTypeEntity existingNotNull = paymentTypeRepository.findByValueAndDeletedAtNotNull(request.getValue());

        PaymentTypeEntity paymentType = new PaymentTypeEntity();
        if (isexists) {
           if(existingNotNull!=null){
               existingNotNull.setDeletedAt(null);
               paymentTypeRepository.save(existingNotNull);
           }else {
               throw new InternalServerError(MessageConstant.Payment_Method.PAYMENTTYPEIDALREAYEXIST);
           }
        } else {
            paymentType.setValue(request.getValue());
            paymentTypeRepository.save(paymentType);
        }
    }

    @Override
    @Transactional
    public void updatePaymentType(Long id, UserPaymentTypeRQ request) {
        PaymentTypeEntity paymentType=paymentTypeRepository.findById(id).orElseThrow(()-> new NotFoundException(MessageConstant.Payment_Method.PAYMENTTYPEIDNOTFOUND));
        paymentType.setValue(request.getValue());
        paymentTypeRepository.save(paymentType);
    }
    @Override
    @Transactional
    public void deletePaymentType(Long id) {
        PaymentTypeEntity paymentType=paymentTypeRepository.findById(id).orElseThrow(()-> new NotFoundException(MessageConstant.Payment_Method.PAYMENTTYPEIDNOTFOUND));
        boolean isexistDelete=paymentTypeRepository.existsByIdAndDeletedAt(id);
        if (isexistDelete){
            throw new InternalServerError(MessageConstant.Payment_Method.PAYMENTTYPEALREADYDELETED);
        }else {
            paymentType.setDeletedAt(Instant.now());
            paymentTypeRepository.save(paymentType);
        }

    }
}
