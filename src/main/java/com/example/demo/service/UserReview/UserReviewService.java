package com.example.demo.service.UserReview;

import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.UserReviewRQ.UpdateUserReviewRQ;
import com.example.demo.model.request.UserReviewRQ.UserReviewRQ;

public interface UserReviewService {
  StructureRS getAllUserReview(Double rating_value,BaseListingRQ request);
  StructureRS createUserreview(UserReviewRQ request);
  void updateUserReview(Long id, UpdateUserReviewRQ request);
  void deleteUserReview(Long id);
}
