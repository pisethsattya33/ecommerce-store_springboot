package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.Orderline.OrderlineRQ;
import com.example.demo.model.request.Orderline.ProductItemRQ;
import com.example.demo.model.request.Orderline.UpdateOrderlineRQ;
import com.example.demo.model.request.Orderline.UpdateStatusOrderlineRQ;
import com.example.demo.service.Orderline.OrderLineService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/orders")
@AllArgsConstructor
public class OrderlineController extends BaseController {
 private final OrderLineService orderLineService;
 @ResponseStatus(HttpStatus.OK)
 @GetMapping
 public ResponseEntity<StructureRS>getALlOrderline(Long order_id,BaseListingRQ request){
    return response(orderLineService.getALlOrderline(order_id,request));
 }

 @ResponseStatus(HttpStatus.CREATED)
 @PostMapping
 public ResponseEntity<StructureRS>createOrderline(@RequestBody @Valid OrderlineRQ request){
     orderLineService.createOrderline(request);
     return response(HttpStatus.CREATED);
 }
 @ResponseStatus(HttpStatus.OK)
 @DeleteMapping("/{id}")
 public ResponseEntity<StructureRS>deleteOrderLine(@PathVariable Long id){
     orderLineService.deleteOrderLine(id);
     return response(HttpStatus.OK);
 }
}
