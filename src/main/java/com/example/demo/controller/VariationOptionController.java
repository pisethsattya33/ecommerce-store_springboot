package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.db.entity.VariationEntity;
import com.example.demo.db.entity.VariationOptionEntity;
import com.example.demo.model.request.variation.CreateVariationOptionRQ;
import com.example.demo.service.variation.VariationService;
import com.example.demo.service.variationOption.VariationOptionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/variationOptions")
@RequiredArgsConstructor
public class VariationOptionController extends BaseController {
    private final VariationOptionService variationOptionService;

    @GetMapping
    public ResponseEntity<StructureRS> getAllVariationOption(BaseListingRQ request) {
        return response(variationOptionService.getAllVariationsOption(request));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StructureRS> getVariationOptionById(@PathVariable Long id, BaseListingRQ request) {
        return response(variationOptionService.getVariationOptionById(id, request));
    }

    @PostMapping()
    public ResponseEntity<StructureRS> createVariationOption(@RequestBody @Valid CreateVariationOptionRQ request) {
        return ResponseEntity.ok(variationOptionService.createVariationOption(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<StructureRS> updateVariationOption(@PathVariable Long id, @RequestBody CreateVariationOptionRQ request) {
//        StructureRS response = variationOptionService.updateVariationOption(id, request);
//        return ResponseEntity.ok(response);
        variationOptionService.updateVariationOption(id, request);
        return response(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<StructureRS> deleteVariationOption(@PathVariable Long id) {
        variationOptionService.deleteVariationOption(id);
        return response(HttpStatus.OK);
    }

}
