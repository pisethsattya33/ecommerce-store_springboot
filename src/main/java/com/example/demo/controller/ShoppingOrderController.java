package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.model.request.ShoppingOrder.ShoppingCartOrderRQ;
import com.example.demo.model.request.ShoppingOrder.ShoppingOrderRQ;
import com.example.demo.model.request.ShoppingOrder.UpdateShoppingOrderRQ;
import com.example.demo.model.request.ShoppingOrder.UpdateShoppingOrderStatus;
import com.example.demo.security.UserPrincipal;
import com.example.demo.service.ShoppingOrder.ShoppingOrderService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/shopping-order")
@AllArgsConstructor
public class ShoppingOrderController extends BaseController {
    private final ShoppingOrderService shoppingOrderService;
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>getShoppingOrder(Long order_id,BaseListingRQ request){
        return response(shoppingOrderService.getShoppingOrder(order_id,request));
    }
    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>getShoppingOrderByUser(JwtAuthenticationToken jwtAuthenticationToken,BaseListingRQ request){
        return response(shoppingOrderService.getShoppingOrderByUser(UserPrincipal.build(jwtAuthenticationToken),request));
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StructureRS>getShoppingOrder(@Valid @RequestBody ShoppingOrderRQ request){
        shoppingOrderService.createShoppingOrder(request);
        return response(HttpStatus.CREATED);
    }
    @PostMapping("/checkout")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<StructureRS>getShoppingOrder(JwtAuthenticationToken jwtAuthenticationToken, @Valid @RequestBody ShoppingCartOrderRQ request){
        shoppingOrderService.createShoppingCartOrder(UserPrincipal.build(jwtAuthenticationToken),request);
        return response(HttpStatus.CREATED);
    }
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>updateShoppingOrder(@Valid @PathVariable Long id, @RequestBody UpdateShoppingOrderRQ request){
        shoppingOrderService.updateShoppingOrder(id,request);
        return response(HttpStatus.OK);
    }
    @PutMapping("/status/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>updateShoppingOrderStatus(@Valid @PathVariable Long id,@RequestBody UpdateShoppingOrderStatus request){
        shoppingOrderService.updateShoppingOrderStatus(id,request);
        return response(HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<StructureRS>deleteShoppingOrder(@PathVariable Long id){
        shoppingOrderService.deleteShoppingOrder(id);
        return response(HttpStatus.OK);
    }

}
