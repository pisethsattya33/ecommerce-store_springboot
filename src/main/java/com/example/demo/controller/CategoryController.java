package com.example.demo.controller;

import com.example.demo.base.BaseController;
import com.example.demo.base.BaseListingRQ;
import com.example.demo.base.StructureRS;
import com.example.demo.constant.MessageConstant;
import com.example.demo.db.entity.CategoryEntity;
import com.example.demo.model.request.category.CategoryDto;
import com.example.demo.model.request.category.UpdateCategoryRQ;
import com.example.demo.model.request.category.UpdateStatusCategoryRQ;
import com.example.demo.service.category.CategoryService;
import jakarta.validation.Valid;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author Sattya
 * create at 2/14/2024 4:33 PM
 */
@RestController
@RequestMapping("/api/v1/categories")
@RequiredArgsConstructor
public class CategoryController extends BaseController {
    private final CategoryService categoryService;
    @GetMapping
    public StructureRS getAll(BaseListingRQ request){
        return response(categoryService.getAll(request)).getBody();
    }

    @GetMapping("/{uuid}")
    public StructureRS findByUuid(@PathVariable String uuid, BaseListingRQ request){
        return response(categoryService.findByUuid(uuid,request)).getBody();
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StructureRS createNew(@RequestBody @Valid CategoryDto request){
       return categoryService.createNew(request);
    }

    @PatchMapping("/{uuid}")
    public StructureRS updateByUuid(@PathVariable String uuid, @RequestBody UpdateCategoryRQ request){
        categoryService.updateByUuid(uuid,request);
        return response(MessageConstant.CATEGORY.CATEGORY_UPDATED_SUCCESSFULLY).getBody();
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping("/{uuid}/status")
    public StructureRS updateStatusCategories(@PathVariable String uuid,@ModelAttribute @RequestBody @Valid UpdateStatusCategoryRQ request){
        return response(categoryService.updateStatusCategories(uuid,request)).getBody();
    }
}
