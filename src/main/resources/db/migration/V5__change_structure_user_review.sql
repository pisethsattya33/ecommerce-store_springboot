ALTER TABLE user_review
DROP
FOREIGN KEY FK_USER_REVIEW_ON_ORDERED_PRODUCT;

CREATE TABLE user_review_products
(
    products_id     BIGINT NOT NULL,
    user_reviews_id BIGINT NOT NULL
);

ALTER TABLE user_review_products
    ADD CONSTRAINT fk_userevpro_on_product_entity FOREIGN KEY (products_id) REFERENCES products (id);

ALTER TABLE user_review_products
    ADD CONSTRAINT fk_userevpro_on_user_review_entity FOREIGN KEY (user_reviews_id) REFERENCES user_review (id);

ALTER TABLE user_review
DROP
COLUMN ordered_product_id;