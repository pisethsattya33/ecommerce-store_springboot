insert into role (name, code, created_by, updated_by, created_at, updated_at)
values ('Super-Admin', 'super-admin', null, null, current_timestamp, current_timestamp);
insert into role (name, code, created_by, updated_by, created_at, updated_at)
values ('Admin', 'admin', null, null, current_timestamp, current_timestamp);
insert into role (name, code, created_by, updated_by, created_at, updated_at)
values ('Customer', 'customer', null, null, current_timestamp, current_timestamp);

insert into permission (id, name, module)
values (1, 'list-role', 'role'),
       (2, 'create-role', 'role'),
       (3, 'edit-role', 'role'),
       (4, 'delete-role', 'role'),
       (5, 'list-user', 'user'),
       (6, 'create-user', 'user'),
       (7, 'edit-user', 'user'),
       (8, 'delete-user', 'user');


insert into roles_has_permissions (permission_id, role_id)
values (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1),
       (8, 1),
       (1, 2),
       (2, 2),
       (3, 2),
       (4, 2),
       (6, 2),
       (7, 2);


insert into users (id, username, password, email, name, bio, avatar, address, phone, status, role_id, created_by,
                   updated_by, created_at, updated_at, uuid)
values (1, 'admin', '{bcrypt}$2a$12$3.ILyOwjOs8yfMIVAKQkW.vrefsv3v9xHxCzBcqlYoOv2r8bFIea.', 'sombath@gmail.com',
        'ADMIN', 'senior back-end developer', null, 'Phnom Penh',
        '0123456789', 1, 1, null, null, current_timestamp, current_timestamp, 'f7e76754-82be-4930-90c2-7fa1966787a6');

insert into categories(id, uuid, name, description, parent_category_id, created_by, updated_by, created_at, updated_at)
values (1, 'f7e76754-82be-4930-90c2-7fa1966787a6', 'Clothing', 'clothing', null, 1, null, current_timestamp,
        current_timestamp),
       (2, 'dec46632-49fb-4df8-b570-2a0783876d65', 'Woman', 'ladies clothing', 1, 1, null, current_timestamp,
        current_timestamp),
       (3, '2626a93d-a62a-42fc-a6c8-01dd8f8b6f46', 'Man', 'mens clothing', 1, 1, null, current_timestamp,
        current_timestamp),
       (4, '22986102-f0fd-49c8-b751-dab4eefd0d05', 'kids', 'kid clothing', 1, 1, null, current_timestamp,
        current_timestamp),
       (5, 'd305e5c4-6788-444e-90aa-4b68300bd8e7', 'Base layer', 'base layer', 3, 1, null, current_timestamp,
        current_timestamp),
       (6, '6c13cbce-b655-4dba-b6dc-ed82d2bbddc9', 'Jackets & Vests', 'jackets & vests', 3, 1, null, current_timestamp,
        current_timestamp),
       (7, '8b42f1bf-24c4-4635-874f-e00df4886221', 'Pants & Leggings', 'pants & leggings', 3, 1, null,
        current_timestamp, current_timestamp),
       (8, 'e82cc36f-9978-4a5d-b53c-3c643717583f', 'Shirts & Tops', 'shirts & tops', 3, 1, null, current_timestamp,
        current_timestamp),
       (9, '06b00098-ae7f-48a3-b47e-2d97093fa7e3', 'Shoes', 'shoes', 3, 1, null, current_timestamp, current_timestamp),
       (10, '8cd4450f-e3d7-433f-afbb-49073ae5f2c3', 'Shorts', 'shorts', 3, 1, null, current_timestamp,
        current_timestamp),
       (11, '65d52c7f-0e6c-488e-8573-82dd0bac809a', 'Underwear', 'underwear', 3, 1, null, current_timestamp,
        current_timestamp),
       (12, '26822ad4-dad8-4913-9375-df799c1c26e9', 'Base layer', 'base layer', 2, 1, null, current_timestamp,
        current_timestamp),
       (13, 'a0a60b16-ebf0-4a1a-ace7-9101530d3c3c', 'Jackets & Vests', 'jackets & vests', 2, 1, null, current_timestamp,
        current_timestamp),
       (14, 'c4be0403-1280-4e12-b186-b0e5180867c0', 'Pants & Leggings', 'pants & leggings', 2, 1, null,
        current_timestamp, current_timestamp),
       (15, 'd1ab52ad-dcbb-48de-a287-dc9d02d2fcc6', 'Shirts & Tops', 'shirts & tops', 2, 1, null, current_timestamp,
        current_timestamp),
       (16, 'b68666bc-a52a-4d2d-853d-f9a2bec088c5', 'Shoes', 'shoes', 2, 1, null, current_timestamp, current_timestamp),
       (17, '711f4e95-00c9-4768-bb62-9cf5ec275970', 'Shorts', 'shorts', 2, 1, null, current_timestamp,
        current_timestamp),
       (18, 'afca717a-5704-48a5-a485-16ced90291e0', 'Dresses & Skirts', 'dresses & skirts', 2, 1, null,
        current_timestamp, current_timestamp),
       (19, '793696fb-94e9-449d-8733-6803fb1929aa', 'Underwear', 'underwear', 2, 1, null, current_timestamp,
        current_timestamp),
       (20, 'd5ad4ea7-8bb5-45c9-a074-d5b30fb9e9c3', 'Base layer', 'base layer', 4, 1, null, current_timestamp,
        current_timestamp),
       (21, '786ad582-673d-4558-b6a8-99f2ef5fb1ae', 'Jackets & Vests', 'jackets & vests', 4, 1, null, current_timestamp,
        current_timestamp),
       (22, 'ecc1bdc0-1523-4678-b7fb-f5217891bce0', 'Pants & Leggings', 'pants & leggings', 4, 1, null,
        current_timestamp, current_timestamp),
       (23, '93e1c311-1cf5-40c5-8eb4-db6f9cdd0782', 'Shirts & Tops', 'shirts & tops', 4, 1, null, current_timestamp,
        current_timestamp),
       (24, '8234af69-526b-4217-abd0-d8412bf40219', 'Shoes', 'shoes', 4, 1, null, current_timestamp, current_timestamp),
       (25, 'cf2fed7b-2f21-4b4c-bdc3-d1fc59194eb8', 'Shorts', 'shorts', 4, 1, null, current_timestamp,
        current_timestamp),
       (26, 'bb70bdaa-64db-47d6-a63a-bbcaade5867a', 'Dresses & Skirts', 'dresses & skirts', 4, 1, null,
        current_timestamp, current_timestamp),
       (27, '98945b84-9a39-4e56-b289-6e6493d866f0', 'Underwear', 'underwear', 4, 1, null, current_timestamp,
        current_timestamp);

insert into variations(id, name, category_id, created_by, updated_by, created_at, updated_at)
values (1, 'Color', 1, 1, null, current_timestamp, current_timestamp),
       (2, 'Size', 1, 1, null, current_timestamp, current_timestamp),
       (3, 'Fit', 1, 1, null, current_timestamp, current_timestamp),
       (4, 'Price', 1, 1, null, current_timestamp, current_timestamp);

insert into variation_options(id, variation_id, value, created_by, updated_by, created_at, updated_at)
values (1, 1, 'Black', 1, null, current_timestamp, current_timestamp),
       (2, 1, 'White', 1, null, current_timestamp, current_timestamp),
       (3, 1, 'Gray', 1, null, current_timestamp, current_timestamp),
       (4, 1, 'Blue', 1, null, current_timestamp, current_timestamp),
       (5, 1, 'Red', 1, null, current_timestamp, current_timestamp),
       (6, 1, 'Green', 1, null, current_timestamp, current_timestamp),
       (7, 1, 'Purple', 1, null, current_timestamp, current_timestamp),
       (8, 1, 'Yellow', 1, null, current_timestamp, current_timestamp),
       (9, 1, 'Pink', 1, null, current_timestamp, current_timestamp),
       (10, 1, 'Orange', 1, null, current_timestamp, current_timestamp),
       (11, 1, 'Brown', 1, null, current_timestamp, current_timestamp),
       (12, 2, 'XS', 1, null, current_timestamp, current_timestamp),
       (13, 2, 'S', 1, null, current_timestamp, current_timestamp),
       (14, 2, 'M', 1, null, current_timestamp, current_timestamp),
       (15, 2, 'L', 1, null, current_timestamp, current_timestamp),
       (16, 2, 'XL', 1, null, current_timestamp, current_timestamp),
       (17, 2, 'XXL', 1, null, current_timestamp, current_timestamp),
       (18, 2, '3XL', 1, null, current_timestamp, current_timestamp),
       (19, 2, '4XL', 1, null, current_timestamp, current_timestamp),
       (20, 3, 'Compression', 1, null, current_timestamp, current_timestamp),
       (21, 3, 'Fitted', 1, null, current_timestamp, current_timestamp),
       (22, 4, 'Under$25', 1, null, current_timestamp, current_timestamp),
       (23, 4, '$25-$50', 1, null, current_timestamp, current_timestamp),
       (24, 4, '$50-$75', 1, null, current_timestamp, current_timestamp),
       (25, 4, '$75-$100', 1, null, current_timestamp, current_timestamp),
       (26, 4, '$100-$200', 1, null, current_timestamp, current_timestamp),
       (27, 4, '$200+', 1, null, current_timestamp, current_timestamp);