ALTER TABLE user_review
DROP
COLUMN rating_value;

ALTER TABLE user_review
    ADD rating_value DOUBLE NULL;

ALTER TABLE user_review
    ADD image VARCHAR(255) NULL;
