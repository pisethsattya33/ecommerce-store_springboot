CREATE TABLE categories
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    uuid               VARCHAR(255) NOT NULL,
    name               VARCHAR(255) NOT NULL,
    `description`      TEXT NULL,
    parent_category_id BIGINT NULL,
    created_at         datetime     NOT NULL,
    updated_at         datetime NULL,
    deleted_at         datetime NULL,
    created_by         BIGINT NULL,
    updated_by         BIGINT NULL,
    CONSTRAINT pk_categories PRIMARY KEY (id)
);

CREATE TABLE order_line
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    product_item_id BIGINT NULL,
    order_id        BIGINT NULL,
    qty             INT NULL,
    price           DECIMAL(10, 2) NOT NULL,
    created_at      datetime       NOT NULL,
    updated_at      datetime NULL,
    deleted_at      datetime NULL,
    created_by      BIGINT NULL,
    updated_by      BIGINT NULL,
    CONSTRAINT pk_order_line PRIMARY KEY (id)
);

CREATE TABLE order_status
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    status     VARCHAR(255) NOT NULL,
    created_at datetime     NOT NULL,
    updated_at datetime NULL,
    deleted_at datetime NULL,
    created_by BIGINT NULL,
    updated_by BIGINT NULL,
    CONSTRAINT pk_order_status PRIMARY KEY (id)
);

CREATE TABLE password_reset_token
(
    id          BIGINT AUTO_INCREMENT NOT NULL,
    token       VARCHAR(255) NULL,
    user_id     BIGINT NOT NULL,
    expiry_date datetime NULL,
    CONSTRAINT pk_password_reset_token PRIMARY KEY (id)
);

CREATE TABLE payment_type
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    value      VARCHAR(255) NOT NULL,
    created_at datetime     NOT NULL,
    updated_at datetime NULL,
    deleted_at datetime NULL,
    created_by BIGINT NULL,
    updated_by BIGINT NULL,
    CONSTRAINT pk_payment_type PRIMARY KEY (id)
);

CREATE TABLE permission
(
    id     BIGINT AUTO_INCREMENT NOT NULL,
    name   VARCHAR(255) NOT NULL,
    module VARCHAR(255) NOT NULL,
    CONSTRAINT pk_permission PRIMARY KEY (id)
);

CREATE TABLE product_configuration
(
    product_item_id     BIGINT NOT NULL,
    variation_option_id BIGINT NOT NULL,
    CONSTRAINT pk_product_configuration PRIMARY KEY (product_item_id, variation_option_id)
);

CREATE TABLE product_items
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    sku           VARCHAR(255)    NOT NULL,
    qty_in_stock  INT            NOT NULL,
    product_image VARCHAR(255) NULL,
    price         DECIMAL(10, 2) NOT NULL,
    product_id    BIGINT NULL,
    created_at    datetime       NOT NULL,
    updated_at    datetime NULL,
    deleted_at    datetime NULL,
    created_by    BIGINT NULL,
    updated_by    BIGINT NULL,
    CONSTRAINT pk_product_items PRIMARY KEY (id)
);

CREATE TABLE products
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    uuid          VARCHAR(255) NOT NULL,
    name          VARCHAR(150) NOT NULL,
    `description` TEXT NULL,
    image_url     VARCHAR(255) NULL,
    category_id   BIGINT NULL,
    created_at    datetime     NOT NULL,
    updated_at    datetime NULL,
    deleted_at    datetime NULL,
    created_by    BIGINT NULL,
    updated_by    BIGINT NULL,
    CONSTRAINT pk_products PRIMARY KEY (id)
);

CREATE TABLE promotion_category
(
    category_id  BIGINT NOT NULL,
    promotion_id BIGINT NOT NULL,
    CONSTRAINT pk_promotion_category PRIMARY KEY (category_id, promotion_id)
);

CREATE TABLE promotions
(
    id            BIGINT AUTO_INCREMENT NOT NULL,
    uuid          VARCHAR(255)  NOT NULL,
    name          VARCHAR(255)  NOT NULL,
    `description` TEXT NULL,
    discount_rate DECIMAL(4, 2) NOT NULL,
    start_date    datetime      NOT NULL,
    end_date      datetime      NOT NULL,
    created_at    datetime      NOT NULL,
    updated_at    datetime NULL,
    deleted_at    datetime NULL,
    created_by    BIGINT NULL,
    updated_by    BIGINT NULL,
    CONSTRAINT pk_promotions PRIMARY KEY (id)
);

CREATE TABLE `role`
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255) NOT NULL,
    code       VARCHAR(255) NULL,
    created_at datetime     NOT NULL,
    updated_at datetime NULL,
    deleted_at datetime NULL,
    created_by BIGINT NULL,
    updated_by BIGINT NULL,
    CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE roles_has_permissions
(
    permission_id BIGINT NOT NULL,
    role_id       BIGINT NOT NULL
);

CREATE TABLE shipping_method
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    name       VARCHAR(255)   NOT NULL,
    price      DECIMAL(10, 2) NOT NULL,
    created_at datetime       NOT NULL,
    updated_at datetime NULL,
    deleted_at datetime NULL,
    created_by BIGINT NULL,
    updated_by BIGINT NULL,
    CONSTRAINT pk_shipping_method PRIMARY KEY (id)
);

CREATE TABLE shop_order
(
    id               BIGINT AUTO_INCREMENT NOT NULL,
    user_id          BIGINT NULL,
    order_date       datetime NULL,
    shipping_method  BIGINT NULL,
    order_total      DECIMAL(10, 2) NOT NULL,
    order_status     BIGINT NULL,
    shipping_address VARCHAR(255)   NOT NULL,
    created_at       datetime       NOT NULL,
    updated_at       datetime NULL,
    deleted_at       datetime NULL,
    created_by       BIGINT NULL,
    updated_by       BIGINT NULL,
    CONSTRAINT pk_shop_order PRIMARY KEY (id)
);

CREATE TABLE shopping_cart
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    user_id    BIGINT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NULL,
    deleted_at datetime NULL,
    created_by BIGINT NULL,
    updated_by BIGINT NULL,
    CONSTRAINT pk_shopping_cart PRIMARY KEY (id)
);

CREATE TABLE shopping_cart_item
(
    id              BIGINT AUTO_INCREMENT NOT NULL,
    qty             INT NULL,
    product_item_id BIGINT NULL,
    cart_id         BIGINT NULL,
    created_at      datetime NOT NULL,
    updated_at      datetime NULL,
    deleted_at      datetime NULL,
    created_by      BIGINT NULL,
    updated_by      BIGINT NULL,
    CONSTRAINT pk_shopping_cart_item PRIMARY KEY (id)
);

CREATE TABLE user_payment_method
(
    id                BIGINT AUTO_INCREMENT NOT NULL,
    user_id           BIGINT NULL,
    payment_type_id   BIGINT NULL,
    provider          VARCHAR(255) NOT NULL,
    account_number    VARCHAR(255) NOT NULL,
    expiry_date       VARCHAR(255) NOT NULL,
    is_default        BIT(1)       NOT NULL,
    payment_method_id BIGINT NULL,
    created_at        datetime     NOT NULL,
    updated_at        datetime NULL,
    deleted_at        datetime NULL,
    created_by        BIGINT NULL,
    updated_by        BIGINT NULL,
    CONSTRAINT pk_user_payment_method PRIMARY KEY (id)
);

CREATE TABLE user_review
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    user_id            BIGINT NULL,
    ordered_product_id BIGINT NULL,
    rating_value       INT NULL,
    comment            VARCHAR(200) NULL,
    created_at         datetime NOT NULL,
    updated_at         datetime NULL,
    deleted_at         datetime NULL,
    created_by         BIGINT NULL,
    updated_by         BIGINT NULL,
    CONSTRAINT pk_user_review PRIMARY KEY (id)
);

CREATE TABLE users
(
    id                 BIGINT AUTO_INCREMENT NOT NULL,
    uuid               VARCHAR(255) NULL,
    username           VARCHAR(255) NOT NULL,
    password           VARCHAR(100) NOT NULL,
    email              VARCHAR(100) NOT NULL,
    name               VARCHAR(100) NULL,
    bio                VARCHAR(200) NULL,
    avatar             VARCHAR(200) NULL,
    address            VARCHAR(200) NULL,
    phone              VARCHAR(50) NULL,
    status             TINYINT(1) DEFAULT 1 NULL,
    `6_digits`         VARCHAR(6) NULL,
    verification_token VARCHAR(255) NULL,
    deleted            BIT(1) DEFAULT 0 NULL,
    role_id            BIGINT       NOT NULL,
    created_at         datetime     NOT NULL,
    updated_at         datetime NULL,
    deleted_at         datetime NULL,
    created_by         BIGINT NULL,
    updated_by         BIGINT NULL,
    CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE TABLE variation_options
(
    id           BIGINT AUTO_INCREMENT NOT NULL,
    value        VARCHAR(255) NOT NULL,
    variation_id BIGINT NULL,
    created_at   datetime     NOT NULL,
    updated_at   datetime NULL,
    deleted_at   datetime NULL,
    created_by   BIGINT NULL,
    updated_by   BIGINT NULL,
    CONSTRAINT pk_variation_options PRIMARY KEY (id)
);

CREATE TABLE variations
(
    id          BIGINT AUTO_INCREMENT NOT NULL,
    name        VARCHAR(255) NOT NULL,
    category_id BIGINT NULL,
    created_at  datetime     NOT NULL,
    updated_at  datetime NULL,
    deleted_at  datetime NULL,
    created_by  BIGINT NULL,
    updated_by  BIGINT NULL,
    CONSTRAINT pk_variations PRIMARY KEY (id)
);

ALTER TABLE categories
    ADD CONSTRAINT uc_categories_uuid UNIQUE (uuid);

ALTER TABLE password_reset_token
    ADD CONSTRAINT uc_password_reset_token_user UNIQUE (user_id);

ALTER TABLE product_items
    ADD CONSTRAINT uc_product_items_sku UNIQUE (sku);

ALTER TABLE products
    ADD CONSTRAINT uc_products_uuid UNIQUE (uuid);

ALTER TABLE promotions
    ADD CONSTRAINT uc_promotions_uuid UNIQUE (uuid);

ALTER TABLE users
    ADD CONSTRAINT uc_users_email UNIQUE (email);

ALTER TABLE users
    ADD CONSTRAINT uc_users_username UNIQUE (username);

ALTER TABLE users
    ADD CONSTRAINT uc_users_uuid UNIQUE (uuid);

ALTER TABLE users
    ADD CONSTRAINT uc_users_verification_token UNIQUE (verification_token);

CREATE INDEX idx_category_name ON categories (name);

CREATE INDEX idx_password_reset_token ON password_reset_token (token);

CREATE INDEX idx_permission_name ON permission (name);

CREATE UNIQUE INDEX idx_product_item_sku ON product_items (sku);

CREATE INDEX idx_product_name ON products (name);

CREATE UNIQUE INDEX idx_product_uuid ON products (uuid);

CREATE INDEX idx_promotion_name ON promotions (name);

CREATE UNIQUE INDEX idx_promotion_uuid ON promotions (uuid);

CREATE UNIQUE INDEX idx_role_name ON `role` (name);

CREATE UNIQUE INDEX idx_user_email ON users (email);

CREATE UNIQUE INDEX idx_user_username ON users (username);

CREATE INDEX idx_variation_name ON variations (name);

CREATE INDEX idx_variation_option_value ON variation_options (value);

ALTER TABLE categories
    ADD CONSTRAINT FK_CATEGORIES_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE categories
    ADD CONSTRAINT FK_CATEGORIES_ON_PARENT_CATEGORY FOREIGN KEY (parent_category_id) REFERENCES categories (id);

ALTER TABLE categories
    ADD CONSTRAINT FK_CATEGORIES_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE order_line
    ADD CONSTRAINT FK_ORDER_LINE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE order_line
    ADD CONSTRAINT FK_ORDER_LINE_ON_ORDER FOREIGN KEY (order_id) REFERENCES shop_order (id);

ALTER TABLE order_line
    ADD CONSTRAINT FK_ORDER_LINE_ON_PRODUCT_ITEM FOREIGN KEY (product_item_id) REFERENCES product_items (id);

ALTER TABLE order_line
    ADD CONSTRAINT FK_ORDER_LINE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE order_status
    ADD CONSTRAINT FK_ORDER_STATUS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE order_status
    ADD CONSTRAINT FK_ORDER_STATUS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE password_reset_token
    ADD CONSTRAINT FK_PASSWORD_RESET_TOKEN_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE payment_type
    ADD CONSTRAINT FK_PAYMENT_TYPE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE payment_type
    ADD CONSTRAINT FK_PAYMENT_TYPE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_CATEGORY FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE products
    ADD CONSTRAINT FK_PRODUCTS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE product_items
    ADD CONSTRAINT FK_PRODUCT_ITEMS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE product_items
    ADD CONSTRAINT FK_PRODUCT_ITEMS_ON_PRODUCT FOREIGN KEY (product_id) REFERENCES products (id);

ALTER TABLE product_items
    ADD CONSTRAINT FK_PRODUCT_ITEMS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE promotions
    ADD CONSTRAINT FK_PROMOTIONS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE promotions
    ADD CONSTRAINT FK_PROMOTIONS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE `role`
    ADD CONSTRAINT FK_ROLE_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE `role`
    ADD CONSTRAINT FK_ROLE_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE shipping_method
    ADD CONSTRAINT FK_SHIPPING_METHOD_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE shipping_method
    ADD CONSTRAINT FK_SHIPPING_METHOD_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE shopping_cart_item
    ADD CONSTRAINT FK_SHOPPING_CART_ITEM_ON_CART FOREIGN KEY (cart_id) REFERENCES shopping_cart (id);

CREATE INDEX idx_shopping_cart_item_cart_id ON shopping_cart_item (cart_id);

ALTER TABLE shopping_cart_item
    ADD CONSTRAINT FK_SHOPPING_CART_ITEM_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE shopping_cart_item
    ADD CONSTRAINT FK_SHOPPING_CART_ITEM_ON_PRODUCT_ITEM FOREIGN KEY (product_item_id) REFERENCES product_items (id);

CREATE INDEX idx_shopping_cart_item_product_item_id ON shopping_cart_item (product_item_id);

ALTER TABLE shopping_cart_item
    ADD CONSTRAINT FK_SHOPPING_CART_ITEM_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE shopping_cart
    ADD CONSTRAINT FK_SHOPPING_CART_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE shopping_cart
    ADD CONSTRAINT FK_SHOPPING_CART_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE shopping_cart
    ADD CONSTRAINT FK_SHOPPING_CART_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

CREATE INDEX idx_shopping_cart_user_id ON shopping_cart (user_id);

ALTER TABLE shop_order
    ADD CONSTRAINT FK_SHOP_ORDER_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE shop_order
    ADD CONSTRAINT FK_SHOP_ORDER_ON_USERID FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE shop_order
    ADD CONSTRAINT FK_SHOP_ORDER_ON_ORDER_STATUS FOREIGN KEY (order_status) REFERENCES order_status (id);

ALTER TABLE shop_order
    ADD CONSTRAINT FK_SHOP_ORDER_ON_SHIPPING_METHOD FOREIGN KEY (shipping_method) REFERENCES shipping_method (id);

ALTER TABLE shop_order
    ADD CONSTRAINT FK_SHOP_ORDER_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_ROLE FOREIGN KEY (role_id) REFERENCES `role` (id);

ALTER TABLE users
    ADD CONSTRAINT FK_USERS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE user_payment_method
    ADD CONSTRAINT FK_USER_PAYMENT_METHOD_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE user_payment_method
    ADD CONSTRAINT FK_USER_PAYMENT_METHOD_ON_PAYMENT_METHOD FOREIGN KEY (payment_method_id) REFERENCES shop_order (id);

ALTER TABLE user_payment_method
    ADD CONSTRAINT FK_USER_PAYMENT_METHOD_ON_PAYMENT_TYPE FOREIGN KEY (payment_type_id) REFERENCES payment_type (id);

ALTER TABLE user_payment_method
    ADD CONSTRAINT FK_USER_PAYMENT_METHOD_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE user_payment_method
    ADD CONSTRAINT FK_USER_PAYMENT_METHOD_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

ALTER TABLE user_review
    ADD CONSTRAINT FK_USER_REVIEW_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE user_review
    ADD CONSTRAINT FK_USER_REVIEW_ON_ORDERED_PRODUCT FOREIGN KEY (ordered_product_id) REFERENCES order_line (id);

CREATE INDEX idx_user_review_ordered_product_id ON user_review (ordered_product_id);

ALTER TABLE user_review
    ADD CONSTRAINT FK_USER_REVIEW_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE user_review
    ADD CONSTRAINT FK_USER_REVIEW_ON_USER FOREIGN KEY (user_id) REFERENCES users (id);

CREATE INDEX idx_user_review_user_id ON user_review (user_id);

ALTER TABLE variations
    ADD CONSTRAINT FK_VARIATIONS_ON_CATEGORY FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE variations
    ADD CONSTRAINT FK_VARIATIONS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE variations
    ADD CONSTRAINT FK_VARIATIONS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE variation_options
    ADD CONSTRAINT FK_VARIATION_OPTIONS_ON_CREATED_BY FOREIGN KEY (created_by) REFERENCES users (id);

ALTER TABLE variation_options
    ADD CONSTRAINT FK_VARIATION_OPTIONS_ON_UPDATED_BY FOREIGN KEY (updated_by) REFERENCES users (id);

ALTER TABLE variation_options
    ADD CONSTRAINT FK_VARIATION_OPTIONS_ON_VARIATION FOREIGN KEY (variation_id) REFERENCES variations (id);

ALTER TABLE promotion_category
    ADD CONSTRAINT fk_procat_on_category_entity FOREIGN KEY (category_id) REFERENCES categories (id);

ALTER TABLE promotion_category
    ADD CONSTRAINT fk_procat_on_promotion_entity FOREIGN KEY (promotion_id) REFERENCES promotions (id);

ALTER TABLE product_configuration
    ADD CONSTRAINT fk_procon_on_product_item_entity FOREIGN KEY (product_item_id) REFERENCES product_items (id);

ALTER TABLE product_configuration
    ADD CONSTRAINT fk_procon_on_variation_option_entity FOREIGN KEY (variation_option_id) REFERENCES variation_options (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_permission_entity FOREIGN KEY (permission_id) REFERENCES permission (id);

ALTER TABLE roles_has_permissions
    ADD CONSTRAINT fk_rolhasper_on_role_entity FOREIGN KEY (role_id) REFERENCES `role` (id);
